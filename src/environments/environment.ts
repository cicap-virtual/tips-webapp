// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  urlAPI: 'http://localhost:5000/api/',
  // urlAPI: 'https://api.cicap.net/api/',
  firebase: {
    apiKey: 'AIzaSyAaOlFRP8hVaaree1Ty4UDz4bgxr76ATUg',
    authDomain: 'cicap-test.firebaseapp.com',
    databaseURL: 'https://cicap-test.firebaseio.com',
    projectId: 'cicap-test',
    storageBucket: 'cicap-test.appspot.com',
    messagingSenderId: '194638751486',
    appId: '1:194638751486:web:ede5dc99645377c8f1a7af',
    measurementId: 'G-4B9NBVGRTM'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
