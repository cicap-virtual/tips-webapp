export const environment = {
  production: true,
  urlAPI: 'https://api.cicap.net/api/',
  firebase: {
    apiKey: 'AIzaSyCiR0qtcrPSpO6lggUSiY9ubQ-EMgtYDtY',
    authDomain: 'cicap-229214.firebaseapp.com',
    databaseURL: 'https://cicap-229214.firebaseio.com',
    projectId: 'cicap-229214',
    storageBucket: 'cicap-229214.appspot.com',
    messagingSenderId: '672695711726',
    appId: '1:672695711726:web:be2d90d55487e749'
  }
};
