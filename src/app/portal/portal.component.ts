import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { Role } from '../shared/models/auth.models';
import { selectRole } from '../store/actions';
import { selectAuthInfo, selectOtherRoles } from '../store/selectors';

@Component({
  selector: 'app-portal',
  templateUrl: './portal.component.html',
  styleUrls: ['./portal.component.sass'],
})
export class PortalComponent implements OnInit {
  roles$ = this.store$.select(selectOtherRoles);
  auth$ = this.store$.select(selectAuthInfo);
  constructor(private store$: Store) {}

  ngOnInit() {}

  public changeRole = (role: Role) =>
    this.store$.dispatch(selectRole({ role }));
}
