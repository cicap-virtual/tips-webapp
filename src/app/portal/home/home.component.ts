import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Schedule } from 'src/app/shared/models/surveys.model';
import { ProfilesService } from 'src/app/shared/services/profiles.service';
import { selectProfile } from 'src/app/store/selectors';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass'],
})
export class HomeComponent implements OnInit {
  pendings$: Observable<Schedule[]>;
  profile$ = this.store$.select(selectProfile);
  constructor(private store$: Store, private profileServ: ProfilesService) {}

  ngOnInit() {
    this.profile$.subscribe({
      next: (profile) => {
        this.pendings$ = this.profileServ.getPendings(profile.id);
      },
      error: (err) => console.error(err),
    });
  }
}
