import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslocoModule } from '@ngneat/transloco';

import { LoadingModalModule } from '../shared/components/loading-modal/loading-modal.module';
import { SurveyCompletionFormModule } from '../shared/components/survey-completion-form/survey-completion-form.module';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { HomeComponent } from './home/home.component';
import { PortalComponent } from './portal.component';
import { PortalRoutingModule } from './portal.routes';
import { SurveyFormComponent } from './survey-form/survey-form.component';

@NgModule({
  declarations: [
    PortalComponent,
    HomeComponent,
    SurveyFormComponent,
    ChangePasswordComponent
  ],
  imports: [
    CommonModule,
    PortalRoutingModule,
    LoadingModalModule,
    FormsModule,
    ReactiveFormsModule,
    SurveyCompletionFormModule,
    TranslocoModule
  ]
})
export class PortalModule {}
