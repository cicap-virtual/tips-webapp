import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslocoService } from '@ngneat/transloco';
import { Observable } from 'rxjs';
import { Completion, Survey } from 'src/app/shared/models/surveys.model';
import { CompletionService } from 'src/app/shared/services/completion.service';
import { SchedulesService } from 'src/app/shared/services/schedules.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-survey-form',
  templateUrl: './survey-form.component.html',
  styleUrls: ['./survey-form.component.sass'],
})
export class SurveyFormComponent implements OnInit {
  survey$: Observable<Survey>;
  scheduleId: string;

  constructor(
    private scheduleServ: SchedulesService,
    private completionServ: CompletionService,
    private transloco: TranslocoService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {
    this.route.params.subscribe({
      next: (params) => {
        this.scheduleId = params['id'];
        this.survey$ = this.scheduleServ.getSurvey(this.scheduleId);
      },
      error: (err) => console.log(err),
    });
  }

  saveCompletion(completion: Completion) {
    completion.scheduleId = this.scheduleId;
    this.completionServ.createCompletion(completion).subscribe({
      next: (res) => {
        swal.fire(
          this.transloco.translate('Encuesta terminada'),
          '',
          'success'
        );
        this.router.navigate(['./'], { relativeTo: this.route.parent.parent });
      },
      error: (err: Error) => {
        console.log(err);
        swal.fire('Algo salió mal', err.message, 'error');
      },
    });
  }
}
