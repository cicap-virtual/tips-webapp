import { Component, ViewContainerRef } from '@angular/core';

import { IdleService } from './shared/services/idle.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
})
export class AppComponent {
  title = 'tips-webapp';

  public viewContainerRef: ViewContainerRef;
  constructor(private idle: IdleService) {
    const service = this.idle;
  }
}
