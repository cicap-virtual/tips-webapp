import { User } from './auth.models';
import { Hierarchy } from './companies.model';

export interface Survey {
  id?: string;
  type?: SurveyType;
  createdBy?: User;
  category?: SurveyCategory;
  measures?: Measure[];
  title?: string;
  description?: string;
  createdDate?: Date;
  creditsCost: number;
  modificateDate?: Date;
  startDate?: Date;
  dueDate?: string;
  public?: boolean;
  final: boolean;
}

export interface Measure {
  id?: string;
  name?: string;
  description?: string;
  subMeasures?: any;
  mainQuestion: Question;
  question?: Question;
  weighting?: number;
  questions?: Question[];
}

export interface Question {
  id: string;
  title?: string;
  text?: string;
  measure: Reference;
  measureQuestion?: boolean;
  reverse?: boolean;
  multiAnswer?: boolean;
  weighting?: number;
  answerSet?: AnswerSet;
}

export interface AnswerSet {
  id?: string;
  name?: string;
  answers?: Answer[];
}

export interface Answer {
  text?: string;
  sort?: number;
  value?: number;
  reverseValue?: number;
}

export interface SurveyType {
  id: string;
  name: string;
  hasRadar: boolean;
  hasBar: boolean;
  hasMeasureQuestion: boolean;
  prefix: string;
  measureName: string;
  wheelCharts: boolean;
  subMeasureName: string;
  instructions: string;
  isRandom: boolean;
  visibleMeasures: boolean;
}

export interface Schedule {
  id?: string;
  survey?: Survey;
  createDate?: Date;
  description?: string;
  startDate?: Date;
  dueDate?: Date;
  participants?: Participant[];
  usersCount?: number;
  active?: boolean;
  assignedBy?: User;
  participantsCount?: number;
  status?: string;
  completed?: number;
  company?: Reference;
  hierarchies?: string[];
}
export interface Participant {
  id?: string;
  name?: string;
  email?: string;
  status?: string;
  completionId: string;
}

export interface Profile {
  id?: string;
  name?: string;
  email?: string;
  mobilePhone?: string;
  officePhone?: string;
  homePhone?: string;
  registerDate?: string;
  company?: Reference;
  position?: Hierarchy;
  user?: Reference;
}

export interface Reference {
  id: string;
  name: string;
  title?: string;
}

export interface Completion {
  id?: string;
  profile?: Reference;
  scheduleId?: string;
  surveyId?: string;
  responses?: Response[];
  measureResponses?: MeasureResponse[];
  completeDate?: Date;
}

export interface Response {
  question?: Question;
  answer?: Answer;
}

export interface MeasureResponse {
  measure?: Reference;
  mainQuestion?: Question;
  answer?: Answer;
  responses?: Response[];
}

export interface SurveyCategory {
  id?: string;
  text?: string;
  value?: number;
  children?: SurveyCategory[];
}
