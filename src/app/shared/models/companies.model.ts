import { Reference } from './surveys.model';

export interface Company {
  id: string;
  name: string;
  fullName: string;
  establishedDate: Date;
  address: string;
  industries: Industry[];
  createDate: Date;
  modificateDate: Date;
}

export interface Division {
  id: string;
  name: string;
  description: string;
}

export interface Department {
  id: string;
  name: string;
  description: string;
  division: Reference;
}

export interface Office {
  id: string;
  name: string;
  description: string;
  department: Reference;
}

export interface Position {
  id: string;
  name: string;
  level: number;
  division: Reference;
  department: Reference;
  office: Reference;
  description: string;
  active: boolean;
}

export interface Industry {
  id: string;
  name: string;
}

export interface Hierarchy {
  id: string;
  companyId: string;
  name: string;
  code: string;
  isPosition: boolean;
  description: string;
  parent?: Reference;
}

export interface Organization {
  id: string;
  name: string;
  code: string;
  isPosition: boolean;
  children: Organization[];
}
