interface HttpResponse {
  version: string;
  statusCode: number;
  requestId: string;
  message: string;
  data?: any;
}
