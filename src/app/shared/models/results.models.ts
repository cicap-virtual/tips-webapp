import { Profile, Reference } from './surveys.model';

export interface SurveyResult {
  survey: Reference;
  completionId: string;
  completeDate: Date;
  profile?: Profile;
  measures: ResultMeasure[];
}

export interface ResultMeasure {
  measureId: string;
  name: string;
  weighting?: number;
  mainQuestion?: ResultQuestion;
  questions: ResultQuestion[];
  value: number;
  subMeasuresValue: number;
}

export interface ResultQuestion {
  questionId: string;
  weighting?: number;
  text: string;
  value: number;
}

export interface Result {
  name: string;
  series: Serie[];
}

export interface Serie {
  value: number;
  name: string;
}
