import { Profile, Reference } from './surveys.model';

export interface User {
  id: string;
  uId: string;
  userName: string;
  email: string;
  companies: Reference[];
  creditsBalance?: number;
  approved?: boolean;
  logged?: boolean;
  roles?: Role[];
  createdDate?: string;
  modificateDate?: string;
}

export interface FirebaseUser {
  uid: string;
  email: string;
  photoURL?: string;
  displayName?: string;
  companies: Reference[];
  roles: Reference[];
}

export interface Role {
  id?: string;
  name?: string;
  isUser?: boolean;
  isAdmin?: boolean;
  isCompanyAdmin?: boolean;
  isCreator?: boolean;
  isExternal?: boolean;
  permissions?: Permission[];
}

export interface Permission {
  access?: Access;
  read?: boolean;
  edit?: boolean;
  create?: boolean;
  delete?: boolean;
}

export interface Access {
  id?: string;
  name?: string;
  code?: string;
}
export interface Session {
  id?: string;
  sessionId?: string;
  loginTime?: Date;
  logoutTime?: Date;
  ip?: string;
  user?: User;
  profile?: Profile;
  browser?: string;
}

export interface Login {
  uId: string;
  email: string;
  displayValue: string;
}

export interface Auth {
  uid: string;
  email: string;
  displayName: string;
  photoURL: string;
  emailVerified: boolean;
}
