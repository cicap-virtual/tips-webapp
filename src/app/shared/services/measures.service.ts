import { Injectable } from '@angular/core';

import { Measure } from '../models/surveys.model';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';

@Injectable({ providedIn: 'root' })
export class MeasuresService {
  private url: string;
  constructor(private http: CustomHttpClient, private conn: ConnectionService) {
    this.url = conn.urlAPI + 'measures';
  }

  public getMeasures() {
    return this.http.get<Measure[]>(this.url);
  }

  public getMeasure(id: string) {
    return this.http.get<Measure>(this.url, id);
  }

  public createMeasure(measure: Measure) {
    return this.http.post<Measure>(this.url, measure);
  }

  public editMeasure(id: string, measure: Measure) {
    return this.http.put(this.url, id, measure);
  }

  public deleteMeasure(id: string) {
    return this.http.delete(this.url, id);
  }
}
