import { Injectable } from '@angular/core';
import { DEFAULT_INTERRUPTSOURCES, Idle } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';

import { AuthService } from './auth.service';

@Injectable({ providedIn: 'root' })
export class IdleService {
  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;
  constructor(
    private idle: Idle,
    private keepalive: Keepalive,
    private auth: AuthService
  ) {
    idle.setIdle(6000);
    if (environment.production) {
      idle.setIdle(60);
    }

    idle.setTimeout(10);
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    idle.onIdleEnd.subscribe({
      next: () => {
        this.idleState = 'No longer idle.';
        console.log(this.idleState);
        this.reset();
      },
    });

    idle.onTimeout.subscribe({
      next: () => {
        Swal.close();
        this.idleState = 'Timed out!';
        this.timedOut = true;
        console.log(this.idleState);
        this.auth.signOut();
      },
    });

    idle.onIdleStart.subscribe({
      next: () => {
        this.idleState = 'You\'ve gone idle!';
        Swal.fire({
          title: 'Aún estás ahí?',
          html: this.idleState,
          icon: 'warning',
          timer: 10000,
          timerProgressBar: true,
          showCancelButton: true,
          cancelButtonText: 'Cerrar sesión',
          confirmButtonText: 'Continuar',
        }).then((res) => {
          if (res.value) {
            this.stay();
          } else {
            this.idleState = 'Timed out!';
            this.timedOut = true;
            console.log(this.idleState);
            this.auth.signOut();
          }
        });
        console.log(this.idleState);
      },
    });

    idle.onTimeoutWarning.subscribe({
      next: (countdown) => {
        this.idleState = `Tu sesión terminará en ${countdown} segundos!`;
        Swal.getHtmlContainer().textContent = this.idleState;
        console.log(this.idleState);
      },
    });
    this.auth.getUserLoggedIn().subscribe({
      next: (userLoggedIn) => {
        console.log(userLoggedIn);
        if (userLoggedIn) {
          idle.watch();
          this.timedOut = false;
        } else {
          idle.stop();
        }
      },
    });

    // sets the ping interval to 15 seconds
    keepalive.interval(15);

    keepalive.onPing.subscribe({ next: () => (this.lastPing = new Date()) });
  }

  reset() {
    this.idle.watch();
    //xthis.idleState = 'Started.';
    this.timedOut = false;
  }

  stay() {
    this.reset();
  }
}
