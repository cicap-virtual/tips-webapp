import { Injectable } from '@angular/core';

import { Division } from '../models/companies.model';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';

@Injectable({ providedIn: 'root' })
export class DivisionsService {
  private url: string;
  constructor(private conn: ConnectionService, private http: CustomHttpClient) {
    this.url = conn.urlAPI + 'divisions';
  }

  public getAll() {
    return this.http.get<Division[]>(this.url);
  }

  public get(id: string) {
    return this.http.get<Division>(this.url, id);
  }

  public create(division: Division) {
    return this.http.post<Division>(this.url, division);
  }

  public edit(id: string, division: Division) {
    return this.http.put(this.url, id, division);
  }

  public delete(id: string) {
    return this.http.delete(this.url, id);
  }
}
