import { Injectable } from '@angular/core';

import { Office } from '../models/companies.model';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';

@Injectable({ providedIn: 'root' })
export class OfficesService {
  private url: string;

  constructor(private http: CustomHttpClient, private conn: ConnectionService) {
    this.url = conn.urlAPI + 'offices';
  }

  public getAll() {
    return this.http.get<Office[]>(this.url);
  }

  public get(id: string) {
    return this.http.get<Office>(this.url, id);
  }

  public create(office: Office) {
    return this.http.post<Office>(this.url, office);
  }

  public edit(id: string, office: Office) {
    return this.http.put(this.url, id, office);
  }

  public delete(id: string) {
    return this.http.delete(this.url, id);
  }
}
