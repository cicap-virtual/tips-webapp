import { Injectable } from '@angular/core';

import { User } from '../models/auth.models';
import { Company, Hierarchy, Organization } from '../models/companies.model';
import { Profile, Schedule } from '../models/surveys.model';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';
import { HierarchyService } from './hierarchy.service';

@Injectable({ providedIn: 'root' })
export class CompaniesService {
  private url: string;

  constructor(private conn: ConnectionService, private http: CustomHttpClient) {
    this.url = conn.urlAPI + 'companies';
  }

  public getAll() {
    return this.http.get<Company[]>(this.url);
  }

  public get(id: string) {
    return this.http.get<Company>(this.url, id);
  }

  public getProfiles(id: string) {
    return this.http.get<Profile[]>(`${this.url}/${id}/profiles`);
  }

  public getAdministrators(id: string) {
    return this.http.get<User[]>(`${this.url}/${id}/administrators`);
  }

  public getHierarchy(id: string) {
    return this.http.get<Hierarchy[]>(`${this.url}/${id}/hierarchy`);
  }

  public getPositions(id: string) {
    return this.http.get<Hierarchy[]>(`${this.url}/${id}/positions`);
  }

  public createProfile(
    id: string,
    item: { name: string; email: string; code?: string }
  ) {
    return this.http.post<Profile>(`${this.url}/profiles/${id}`, item);
  }

  public getParents(id: string) {
    return this.http.get<HierarchyService[]>(`${this.url}/${id}/parents`);
  }
  public getSchedules(id: string) {
    return this.http.get<Schedule[]>(`${this.url}/${id}/schedules`);
  }

  public getOrganization(id: string) {
    return this.http.get<Organization[]>(`${this.url}/${id}/organization`);
  }

  public create(company: Company) {
    return this.http.post<Company>(this.url, company);
  }

  public edit(id: string, company: Company) {
    return this.http.put(this.url, id, company);
  }

  public delete(id: string) {
    return this.http.delete(this.url, id);
  }
}
