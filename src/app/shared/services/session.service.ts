import { Injectable } from '@angular/core';

import { Session, User } from '../models/auth.models';

@Injectable({ providedIn: 'root' })
export class SessionService {
  public get userInfo(): User {
    const session: Session = JSON.parse(localStorage.getItem('session'));
    return session.user;
  }

  public get currentSession() {
    return JSON.parse(localStorage.getItem('session'));
  }

  public set currentSession(session: Session) {
    localStorage.setItem('session', JSON.stringify(session));
  }
}
