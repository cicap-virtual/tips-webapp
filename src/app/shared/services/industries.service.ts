import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';
import { Industry } from '../models/companies.model';

@Injectable({ providedIn: 'root' })
export class IndustriesService {
  private url: string;

  constructor(private conn: ConnectionService, private http: CustomHttpClient) {
    this.url = conn.urlAPI + 'industries';
  }

  public getAll() {
    return this.http.get<Industry[]>(this.url);
  }

  public get(id: string) {
    return this.http.get<Industry>(this.url, id);
  }

  public create(industry: Industry) {
    return this.http.post<Industry>(this.url, industry);
  }

  public edit(id: string, industry: Industry) {
    return this.http.put(this.url, id, industry);
  }

  public delete(id: string) {
    return this.http.delete(this.url, id);
  }
}
