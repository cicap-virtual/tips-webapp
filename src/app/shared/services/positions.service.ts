import { Injectable } from '@angular/core';

import { Position } from '../models/companies.model';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';

@Injectable({ providedIn: 'root' })
export class PositionsService {
  private url: string;
  constructor(private conn: ConnectionService, private http: CustomHttpClient) {
    this.url = conn.urlAPI + 'positions';
  }
  public getAll() {
    return this.http.get<Position[]>(this.url);
  }

  public get(id: string) {
    return this.http.get<Position>(this.url, id);
  }
  public create(position: Position) {
    return this.http.post<Position>(this.url, position);
  }
  public edit(id: string, position: Position) {
    return this.http.put(this.url, id, position);
  }
  public delete(id: string) {
    return this.http.delete(this.url, id);
  }
}
