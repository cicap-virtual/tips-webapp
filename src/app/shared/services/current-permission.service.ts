import { Injectable } from '@angular/core';
import { map, mergeMap } from 'rxjs';
import { StorageService } from './storage.service';
import { filter } from 'lodash';
@Injectable({
  providedIn: 'root',
})
export class CurrentPermissionsService {
  constructor(private storage: StorageService) {}
}
