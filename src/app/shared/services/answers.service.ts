import { Injectable } from '@angular/core';

import { AnswerSet } from '../models/surveys.model';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';

@Injectable({ providedIn: 'root' })
export class AnswersService {
  private url: string;
  constructor(
    private http: CustomHttpClient,
    private conn: ConnectionService
  ) {
    this.url = this.conn.urlAPI + 'answersets';
  }

  public getAnswerSets() {
    return this.http.get<AnswerSet[]>(this.url);
  }

  public getAnswerSet(id: string) {
    return this.http.get<AnswerSet>(this.url, id);
  }

  public createAnswerSet(set: AnswerSet) {
    return this.http.post(this.url, set);
  }

  public editAnswerSet(id: string, set: AnswerSet) {
    return this.http.put(this.url, id, set);
  }

  public deleteAnswerSet(id: string) {
    return this.http.delete(this.url, id);
  }
}
