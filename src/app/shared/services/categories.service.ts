import { Injectable } from '@angular/core';
import { SurveyCategory } from '../models/surveys.model';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';

@Injectable({ providedIn: 'root' })
export class CategoriesService {
  private url: string;
  constructor(private http: CustomHttpClient, private conn: ConnectionService) {
    this.url = conn.urlAPI + 'categories/';
  }

  public getAll() {
    return this.http.get<SurveyCategory[]>(this.url);
  }

  public get(id: string) {
    return this.http.get<SurveyCategory>(this.url, id);
  }

  public create(category: SurveyCategory) {
    return this.http.post<SurveyCategory>(this.url, category);
  }

  public edit(id: string, category: SurveyCategory) {
    return this.http.put(this.url, id, category);
  }

  public delete(id: string) {
    return this.http.delete(this.url, id);
  }
}
