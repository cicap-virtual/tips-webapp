import { Injectable } from '@angular/core';
import { mergeMap } from 'rxjs';

import { Survey } from '../models/surveys.model';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';
import { StorageService } from './storage.service';

@Injectable({ providedIn: 'root' })
export class SurveysService {
  private url: string;
  constructor(
    private http: CustomHttpClient,
    private storage: StorageService,
    private conn: ConnectionService
  ) {
    this.url = this.conn.urlAPI + 'surveys';
  }

  public getSurveys() {
    return this.http.get<Survey[]>(this.url);
  }

  public getCount() {
    return this.http.get<number>(this.url + '/count');
  }

  public getSurvey(id: string) {
    return this.http.get<Survey>(this.url, id);
  }

  public getTemp() {
    return this.storage.currentSession$.pipe(
      mergeMap((session) =>
        this.http.get<Survey[]>(`${this.url}/${session.user.id}/temp`)
      )
    );
  }

  public saveTemp(survey: Survey) {
    return this.storage.currentSession$.pipe(
      mergeMap((session) => {
        survey.createdBy = session.user;
        return this.http.post<Survey>(`${this.url}/temp`, survey);
      })
    );
  }

  public createSurvey(survey: Survey) {
    return this.storage.currentSession$.pipe(
      mergeMap((session) => {
        survey.createdBy = session.user;
        return this.http.post<Survey>(this.url, survey);
      })
    );
  }

  public editSurvey(id: string, survey: Survey) {
    return this.storage.currentSession$.pipe(
      mergeMap((session) => {
        survey.createdBy = session.user;
        return this.http.put(this.url, id, survey);
      })
    );
  }

  public deleteSurvey(id: string) {
    return this.http.delete(this.url, id);
  }
}
