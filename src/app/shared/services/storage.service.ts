/* eslint-disable @typescript-eslint/member-ordering */
import { Injectable } from '@angular/core';
import { StorageMap } from '@ngx-pwa/local-storage';
import { mergeMap, Observable } from 'rxjs';

import { Session } from '../models/auth.models';
import { Profile } from '../models/surveys.model';

@Injectable({ providedIn: 'root' })
export class StorageService {
  constructor(private storage: StorageMap) {}

  get currentProfile$() {
    return this.storage.get('profile') as Observable<Profile>;
  }

  set currentProfile$(profile$: Observable<Profile>) {
    profile$
      .pipe(mergeMap((profile) => this.storage.set('profile', profile)))
      .subscribe({
        next: () => {},
        error: (err: Error) => {
          console.log(err);
        },
      });
  }

  get currentSession$() {
    return this.storage.get('session') as Observable<Session>;
  }

  set currentSession$(session$: Observable<Session>) {
    session$
      .pipe(mergeMap((session) => this.storage.set('session', session)))
      .subscribe({
        next: () => {},
        error: (err: Error) => {
          console.log('session failed', err);
        },
      });
  }

  clean(key: string) {
    this.storage.delete(key).subscribe({ next: () => {} });
  }

  clear() {
    this.storage.clear().subscribe({ next: () => {} });
  }
}
