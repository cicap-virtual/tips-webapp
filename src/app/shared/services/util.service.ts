import { Injectable } from '@angular/core';
import * as _ from 'underscore';

@Injectable({ providedIn: 'root' })
export class UtilService {
  constructor() {}
  get months() {
    return [
      { id: 1, name: 'Enero', shortName: 'Ene' },
      { id: 2, name: 'Febrero', shortName: 'Feb' },
      { id: 3, name: 'Marzo', shortName: 'Mar' },
      { id: 4, name: 'Abril', shortName: 'Abr' },
      { id: 5, name: 'Mayo', shortName: 'May' },
      { id: 6, name: 'Junio', shortName: 'Jun' },
      { id: 7, name: 'Julio', shortName: 'Jul' },
      { id: 8, name: 'Agosto', shortName: 'Ago' },
      { id: 9, name: 'Septiembre', shortName: 'Sep' },
      { id: 10, name: 'Octubre', shortName: 'Oct' },
      { id: 11, name: 'Noviembre', shortName: 'Nov' },
      { id: 12, name: 'Diciembre', shortName: 'Dic' }
    ];
  }

  sortBy(array: Array<any>, args: string, desc?: boolean): Array<any> {
    if (desc) {
      array.sort((a: any, b: any) => {
        if (!a[args]) {
          return 1;
        } else if (!b[args]) {
          return -1;
        } else if (a[args] < b[args]) {
          return 1;
        } else if (a[args] > b[args]) {
          return -1;
        } else {
          return 0;
        }
      });
    } else {
      array.sort((a: any, b: any) => {
        if (!a[args]) {
          return -1;
        } else if (!b[args]) {
          return 1;
        } else if (a[args] < b[args]) {
          return -1;
        } else if (a[args] > b[args]) {
          return 1;
        } else {
          return 0;
        }
      });
    }
    return array;
  }

  existIn(array: any[], property: string, val: any): boolean {
    array.forEach(element => {
      if (element[property] === val) {
        return true;
      }
    });
    return false;
  }

  filterByID(array: Array<any>, id: string): any {
    return array.find(item => item.id === id);
  }

  removeById(array: Array<any>, id: string): Array<any> {
    return array.filter(item => item.id !== id);
  }

  mapDistinct(array: any[], field: string): any[] {
    const items = array.map(x => x[field]);
    const result = [];
    items.forEach(element => {
      if (!result.includes(element)) {
        result.push(element);
      }
    });
    return result;
  }

  mapCountItems(array: any[], field: string): any[] {
    const items = array.map(x => this.getProperty(x, field));
    const result = [];
    items.forEach(element => {
      if (result.filter(e => e.name === element).length === 0) {
        const item: any = {};
        item.name = element;
        item.count = 1;
        result.push(item);
      } else {
        const item = result.find(e => e.name === element);
        item.count++;
      }
    });
    return result;
  }

  mapCount(array: any[], field: string): any {
    const items = array.map(x => this.getProperty(x, field));
    const result = {};
    items.forEach(element => {
      if (!result[element]) {
        result[element] = 1;
      } else {
        result[element]++;
      }
    });
    return result;
  }

  searchFilter(
    array: Array<any>,
    args: Array<string>,
    searchText: string
  ): Array<any> {
    const filterArray: Array<any> = [];

    searchText = searchText.toLocaleLowerCase();

    for (const item of array) {
      let term = '';
      for (const col of args) {
        term = term + this.isNullString(this.getProperty(item, col));
      }
      term = term.toLocaleLowerCase();
      if (term.indexOf(searchText) >= 0) {
        filterArray.push(item);
      }
    }
    return filterArray;
  }

  getAge(birthdate: Date): number {
    const NOW = new Date();
    const date = new Date(birthdate);
    return NOW.getFullYear() - date.getFullYear();
  }

  isNullString(str?: string): string {
    return str || '';
  }

  average(array: number[]) {
    let sum = 0;
    const count = array.length;
    for (let i = 0; i < count; i++) {
      sum = sum + array[i];
    }
    return sum / count;
  }

  capitalize(str: string): string {
    return str.replace(/\b\w/g, l => l.toUpperCase());
  }

  getBase64ImageFromURL(url) {
    return new Promise((resolve, reject) => {
      const img = new Image();
      img.setAttribute('crossOrigin', 'anonymous');
      img.onload = () => {
        const canvas = document.createElement('canvas');
        canvas.width = img.width;
        canvas.height = img.height;
        const ctx = canvas.getContext('2d');
        ctx.drawImage(img, 0, 0);
        const dataURL = canvas.toDataURL('image/png');
        resolve(dataURL);
      };
      img.onerror = error => {
        reject(error);
      };
      img.src = url;
    });
  }

  paginate(itemsCount: number, currentPage: number = 1, pageSize: number = 10) {
    const totalPages: number = Math.ceil(itemsCount / pageSize) + 1;
    let startPage: number;
    let endPage: number;

    if (totalPages <= 10) {
      startPage = 1;
      endPage = totalPages;
    } else {
      if (currentPage <= 6) {
        startPage = 1;
        endPage = 10;
      } else if (currentPage + 4 >= totalPages) {
        startPage = totalPages - 9;
        endPage = totalPages;
      } else {
        startPage = currentPage - 5;
        endPage = currentPage + 4;
      }
    }

    const startIndex = (currentPage - 1) * pageSize;
    const endIndex = Math.min(startIndex + pageSize - 1, itemsCount - 1);

    const pages = _.range(startPage, endPage);

    return {
      totalItems: itemsCount,
      currentPage,
      pageSize,
      totalPages,
      startPage,
      endPage,
      startIndex,
      endIndex,
      pages
    };
  }

  updateItem(item: any, array: any[]): any[] {
    const objIndex = array.findIndex(obj => obj.ID === item.ID);
    array[objIndex] = item;
    return array;
  }

  getProperty(item: any, property: string): any {
    property.split('.').forEach(e => {
      item = item ? item[e] : '';
    });
    return item;
  }

  convertToCSV(objArray) {
    const array =
      typeof objArray !== 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    let row = '';

    objArray.forEach(index => {
      row += index + ',';
    });

    row = row.slice(0, -1);
    str += row + '\r\n';
    // eslint-disable-next-line @typescript-eslint/prefer-for-of
    for (let i = 0; i < array.length; i++) {
      let line = '';

      // eslint-disable-next-line guard-for-in
      for (const index in array[i]) {
        if (line !== '') {
          line += ',';
        }
        line += array[i][index];
      }

      str += line + '\r\n';
    }
    return str;
  }
}
