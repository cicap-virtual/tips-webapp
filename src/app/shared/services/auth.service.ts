import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';
import { TranslocoService } from '@ngneat/transloco';
import { Store } from '@ngrx/store';
import { StorageMap } from '@ngx-pwa/local-storage';
import auth from 'firebase/compat/app';
import { firstValueFrom, Observable, of, Subject, Subscription } from 'rxjs';
import { loginSuccess } from 'src/app/store/actions';
import Swal from 'sweetalert2';

import { Auth, Login, Session } from '../models/auth.models';
import { ConnectionService } from './connection.service';
import { StorageService } from './storage.service';

@Injectable({ providedIn: 'root' })
export class AuthService {
  userData: auth.User;
  user: Subscription;
  private userLoggedIn$ = new Subject<boolean>();
  private headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(
    public afs: AngularFirestore,
    private store$: Store,
    public afAuth: AngularFireAuth,
    public router: Router,
    private ngZone: NgZone,
    private transloco: TranslocoService,
    private http: HttpClient,
    private storage: StorageMap,
    private storageServ: StorageService,
    private conn: ConnectionService
  ) {
    this.user = this.afAuth.authState.subscribe({
      next: (user) => {
        if (user) {
          this.userData = user;
        }
      },
      error: (err) => console.error(err),
    });
  }

  inviteUser(email: string) {
    this.afAuth.sendSignInLinkToEmail(email, {
      url: window.location.origin,
    });
  }

  setUserLoggedIn(userLoggedIn: boolean) {
    this.userLoggedIn$.next(userLoggedIn);
  }

  getUserLoggedIn(): Observable<boolean> {
    return this.userLoggedIn$;
  }

  async setCurrentSession() {
    return await firstValueFrom(
      this.login({
        uId: this.userData.uid,
        email: this.userData.email,
        displayValue: this.userData.displayName,
      })
    )
      .then((session) => {
        this.storageServ.currentSession$ = of(session);
        Swal.fire({
          toast: true,
          text: this.transloco.translate('Welcome', {
            name: session.user.userName,
          }),
          timer: 3000,
          icon: 'success',
          showConfirmButton: false,
          position: 'top-right',
        });
        this.setUserLoggedIn(true);
      })
      .catch((err: Error) => {
        console.log(err.message);
      });
  }

  async signIn(email: string, password: string) {
    return await this.afAuth
      .signInWithEmailAndPassword(email, password)
      .then(async (res) => {
        this.setUserData(res.user).then(() => {
          Swal.fire({
            title: 'Iniciando sesión',
            html: 'Cargando',
            allowOutsideClick: false,
            didOpen: () => Swal.showLoading(),
          });
          this.login({
            uId: this.userData.uid,
            email: this.userData.email,
            displayValue: this.userData.displayName,
          }).subscribe({
            next: (session) => {
              const { uid, displayName, photoURL, emailVerified } =
                this.userData;
              this.store$.dispatch(
                loginSuccess({
                  user: session.user,
                  profile: session.profile,
                  authInfo: {
                    uid,
                    displayName,
                    email,
                    photoURL,
                    emailVerified,
                  },
                })
              );
              this.setUserLoggedIn(true);
              this.ngZone.run(() => {
                Swal.close();
              });
            },
          });
        });
      })
      .catch((err) => {
        Swal.fire(
          this.transloco.translate('Something went wrong'),
          this.transloco.translate(err.message),
          'error'
        );
      });
  }

  async signUp(email: string, password: string) {
    return this.afAuth
      .createUserWithEmailAndPassword(email, password)
      .then(async (res) => {
        await this.sendVerificationMail();
        this.setUserData(res.user);
      })
      .catch((err) => {
        Swal.fire(
          this.transloco.translate('Something went wrong'),
          this.transloco.translate(err.message),
          'error'
        );
      });
  }

  async sendVerificationMail() {
    return this.afAuth.currentUser.then((x) =>
      x.sendEmailVerification().then(() => {
        Swal.fire(
          'Verificación enviada',
          'Un enlace fue enviado a su correo electrónico',
          'success'
        );
        this.router.navigate(['./']);
      })
    );
  }

  async forgotPassword(email: string) {
    return this.afAuth
      .sendPasswordResetEmail(email)
      .then(() => {
        Swal.fire(
          this.transloco.translate('Password reset'),
          this.transloco.translate(
            'Password reset email sent, check your inbox.'
          ),
          'info'
        );
      })
      .catch((err) => {
        window.alert(err);
      });
  }

  async isLoggedIn(): Promise<boolean> {
    const session = await firstValueFrom(this.storageServ.currentSession$);
    return session !== null;
  }

  login(login: Login) {
    return this.http.post<Session>(`${this.conn.urlAPI}auth/login`, login, {
      headers: this.headers,
    });
  }

  googleAuth() {
    return this.authLogin(new auth.auth.GoogleAuthProvider());
  }

  async authLogin(provider: any) {
    return await this.afAuth
      .signInWithPopup(provider)
      .then(async (result) => {
        this.setUserData(result.user).then(() => {
          Swal.fire({
            title: 'Iniciando sesión',
            html: 'Cargando',
            allowOutsideClick: false,
            didOpen: () => Swal.showLoading(),
          });
          this.login({
            uId: this.userData.uid,
            email: this.userData.email,
            displayValue: this.userData.displayName,
          }).subscribe({
            next: (session) => {
              const { uid, displayName, email, photoURL, emailVerified } =
                this.userData;
              this.store$.dispatch(
                loginSuccess({
                  user: session.user,
                  profile: session.profile,
                  authInfo: {
                    uid,
                    displayName,
                    email,
                    photoURL,
                    emailVerified,
                  },
                })
              );
              this.setUserLoggedIn(true);
              this.ngZone.run(() => {
                Swal.close();
              });
            },
          });
        });
      })
      .catch((error) => {
        console.log(error);
        Swal.fire(
          this.transloco.translate('Something went wrong'),
          this.transloco.translate(error.message),
          'error'
        );
      });
  }

  async setUserData(user: Auth) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(
      `users/${user.uid}`
    );
    const userData: Auth = {
      uid: user.uid,
      email: user.email,
      displayName: user.displayName,
      photoURL: user.photoURL,
      emailVerified: user.emailVerified,
    };
    return userRef.set(userData, {
      merge: true,
    });
  }

  public async isLogged() {
    const session = await firstValueFrom(this.storageServ.currentSession$);
    if (session) {
      return true;
    } else {
      return false;
    }
  }

  public hasAccess(
    permission: 'read' | 'edit' | 'create' | 'delete',
    access?: string
  ): boolean {
    // let result = false;
    // if (!access) {
    //   result = true;
    // } else {
    //   for (const role of this.sessionServ.userInfo.roles) {
    //     if (
    //       role.permissions.filter(
    //         x => x.access.code === access && x[permission] === true
    //       ).length > 0
    //     ) {
    //       result = true;
    //       break;
    //     }
    //   }
    // }
    return true;
  }

  public isOwner(item: any) {
    this.storageServ.currentSession$.subscribe({
      next: (session) => {
        if (item.createdBy) {
          if (item.createdBy.id === session.user.id) {
            return true;
          }
        } else {
          return false;
        }
      },
    });
  }

  async signOut() {
    console.log('sign-out');
    await this.afAuth.signOut().then(() => {
      this.storageServ.currentSession$.subscribe({
        next: (session) =>
          this.http.post(`${this.conn.urlAPI}auth/logout`, session, {
            headers: this.headers,
          }),
        error: (err) => console.error(err),
      });
      this.storage.clear().subscribe({
        next: () => {
          this.setUserLoggedIn(false);
          this.router.navigate(['/']);
        },
      });
    });
  }
}
