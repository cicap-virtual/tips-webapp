import { Injectable } from '@angular/core';
import { Survey } from '../models/surveys.model';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';

@Injectable({ providedIn: 'root' })
export class MarketService {
  private url: string;
  constructor(private http: CustomHttpClient, private conn: ConnectionService) {
    this.url = conn.urlAPI + 'market/';
  }

  public getCataloge() {
    return this.http.get<Survey[]>(this.url + 'surveys');
  }
}
