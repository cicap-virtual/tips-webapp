import { Injectable } from '@angular/core';

import { SurveyResult } from '../models/results.models';
import { Completion } from '../models/surveys.model';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';

@Injectable({ providedIn: 'root' })
export class CompletionService {
  private url: string;
  constructor(private http: CustomHttpClient, private conn: ConnectionService) {
    this.url = conn.urlAPI + 'completions';
  }

  public getCompletions() {
    return this.http.get<Completion[]>(this.url);
  }

  public getCompletion(id: string) {
    return this.http.get<Completion>(this.url, id);
  }

  public getResult(id: string) {
    return this.http.get<SurveyResult>(`${this.url}/${id}/results`);
  }

  public createCompletion(completion: Completion) {
    return this.http.post<Completion>(this.url, completion);
  }

  public deleteCompletion(id: string) {
    return this.http.delete(this.url, id);
  }
}
