import { Injectable } from '@angular/core';

import { Role } from '../models/auth.models';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';

@Injectable({ providedIn: 'root' })
export class RolesService {
  private url: string;
  constructor(private http: CustomHttpClient, private conn: ConnectionService) {
    this.url = this.conn.urlAPI + 'roles';
  }

  public getRoles() {
    return this.http.get<Role[]>(this.url);
  }

  public getRole(id: string) {
    return this.http.get<Role>(this.url, id);
  }

  public createRole(role: Role) {
    return this.http.post<Role>(this.url, role);
  }

  public editRole(id: string, role: Role) {
    return this.http.put(this.url, id, role);
  }

  public deleteRole(id: string) {
    return this.http.delete(this.url, id);
  }
}
