import { Injectable } from '@angular/core';
import { mergeMap } from 'rxjs';

import { User } from '../models/auth.models';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';
import { StorageService } from './storage.service';

@Injectable({ providedIn: 'root' })
export class UsersService {
  private url: string;
  constructor(
    private storage: StorageService,
    private http: CustomHttpClient,
    private conn: ConnectionService
  ) {
    this.url = this.conn.urlAPI + 'users';
  }

  public getUsers() {
    return this.http.get<User[]>(this.url);
  }

  public changePassword(password: string) {
    return this.storage.currentSession$.pipe(
      mergeMap((session) => {
        const object = {
          id: session.user.id,
          password,
        };
        return this.http.post(`${this.url}/change-password`, object);
      })
    );
  }

  public getUser(id: string) {
    return this.http.get<User>(this.url, id);
  }

  public getByEmail(email: string) {
    return this.http.get<User>(`${this.url}/byemail`, email);
  }

  public createUser(user: User) {
    return this.http.post<User>(this.url, user);
  }

  public editUser(id: string, user: User) {
    return this.http.put(this.url, id, user);
  }

  public deleteUser(id: string) {
    return this.http.delete(this.url, id);
  }
}
