import { Injectable } from '@angular/core';

import { Access } from '../models/auth.models';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';

@Injectable({ providedIn: 'root' })
export class PermissionsService {
  private url: string;
  constructor(private http: CustomHttpClient, private conn: ConnectionService) {
    this.url = this.conn.urlAPI + 'permissions';
  }

  public getPermissions() {
    return this.http.get<Access[]>(this.url);
  }

  public getPermission(id: string) {
    return this.http.get<Access>(this.url, id);
  }

  public createPermission(permission: Access) {
    return this.http.post<Access>(this.url, permission);
  }

  public editPermission(id: string, permission: Access) {
    return this.http.put(this.url, id, permission);
  }

  public deletePermission(id: string) {
    return this.http.delete(this.url, id);
  }
}
