import { Injectable } from '@angular/core';

import { Profile, Schedule } from '../models/surveys.model';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';

@Injectable({ providedIn: 'root' })
export class ProfilesService {
  private url: string;
  constructor(private http: CustomHttpClient, private conn: ConnectionService) {
    this.url = this.conn.urlAPI + 'profiles';
  }

  public getProfiles() {
    return this.http.get<Profile[]>(this.url);
  }

  public getProfile(id: string) {
    return this.http.get<Profile>(this.url, id);
  }

  public getCount() {
    return this.http.get<number>(this.url + '/count');
  }

  public getPendings(id: string) {
    return this.http.get<Schedule[]>(`${this.url}/${id}/pendings`);
  }

  public getUserProfile(id: string) {
    return this.http.get<Profile>(`${this.url}/${id}/user`);
  }

  public createProfile(profile: Profile) {
    return this.http.post<Profile>(this.url, profile);
  }

  public editProfile(id: string, profile: Profile) {
    return this.http.put(this.url, id, profile);
  }

  public deleteProfile(id: string) {
    return this.http.delete(this.url, id);
  }
}
