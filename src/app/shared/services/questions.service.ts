import { Injectable } from '@angular/core';

import { Question } from '../models/surveys.model';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';

@Injectable({ providedIn: 'root' })
export class QuestionsService {
  private url: string;
  constructor(private http: CustomHttpClient, private conn: ConnectionService) {
    this.url = this.conn.urlAPI + 'questions';
  }

  public getQuestions() {
    return this.http.get<Question[]>(this.url);
  }

  public getQuestion(id: string) {
    return this.http.get(this.url, id);
  }

  public createQuestion(question: Question) {
    return this.http.post<Question>(this.url, question);
  }

  public editQuestion(id: string, question: Question) {
    return this.http.put(this.url, id, question);
  }

  public deleteQuestion(id: string) {
    return this.http.delete(this.url, id);
  }
}
