import { Injectable } from '@angular/core';

import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root',
})
export class RoutesService {
  constructor(private readonly session: SessionService) {}

  hasAccess(
    permission: 'read' | 'edit' | 'create' | 'delete',
    access?: string
  ): boolean {
    if (!access) {
      return true;
    }

    for (const role of this.session.userInfo.roles) {
      if (
        role.permissions.filter(
          (x) => x.access.code === access && x[permission] === true
        ).length > 0
      ) {
        return true;
      }
    }
    return false;
  }
}
