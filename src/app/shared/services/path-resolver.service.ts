import { Injectable } from '@angular/core';

export interface PathResolversParams<T> {
  params: T;
}

@Injectable({ providedIn: 'root' })
export class PathResolverService {
  constructor() {}
}
