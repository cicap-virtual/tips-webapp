import { Injectable } from '@angular/core';

import { Department } from '../models/companies.model';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';

@Injectable({ providedIn: 'root' })
export class DepartmentsService {
  private url: string;
  constructor(private conn: ConnectionService, private http: CustomHttpClient) {
    this.url = conn.urlAPI + 'departments';
  }

  public getAll() {
    return this.http.get<Department[]>(this.url);
  }

  public get(id: string) {
    return this.http.get<Department>(this.url, id);
  }

  public create(department: Department) {
    return this.http.post<Department>(this.url, department);
  }

  public edit(id: string, department: Department) {
    return this.http.put(this.url, id, department);
  }

  public delete(id: string) {
    return this.http.delete(this.url, id);
  }
}
