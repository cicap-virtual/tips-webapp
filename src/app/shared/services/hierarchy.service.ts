import { Injectable } from '@angular/core';
import { Hierarchy } from '../models/companies.model';
import { Profile } from '../models/surveys.model';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';

@Injectable({ providedIn: 'root' })
export class HierarchyService {
  private url: string;

  constructor(private conn: ConnectionService, private http: CustomHttpClient) {
    this.url = this.conn.urlAPI + 'hierarchies';
  }

  public getAll() {
    return this.http.get<Hierarchy[]>(this.url);
  }

  public get(id: string) {
    return this.http.get<Hierarchy>(this.url, id);
  }

  public edit(id: string, hierarchy: Hierarchy) {
    return this.http.put(this.url, id, hierarchy);
  }

  public getEmployees(id: string, profileIds: string[]) {
    return this.http.post<Profile[]>(this.url + '/employees', profileIds);
  }

  public create(hierarchy: Hierarchy) {
    return this.http.post<Hierarchy>(this.url, hierarchy);
  }

  public delete(id: string) {
    return this.http.delete(this.url, id);
  }
}
