import { Injectable } from '@angular/core';
import { mergeMap } from 'rxjs';

import { SurveyResult } from '../models/results.models';
import { Completion, Schedule, Survey } from '../models/surveys.model';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';
import { StorageService } from './storage.service';

@Injectable({ providedIn: 'root' })
export class SchedulesService {
  private url: string;
  constructor(
    private http: CustomHttpClient,
    private storage: StorageService,
    private conn: ConnectionService
  ) {
    this.url = this.conn.urlAPI + 'schedules';
  }

  public getSchedules() {
    return this.http.get<Schedule[]>(this.url);
  }

  public getSurvey(id: string) {
    return this.http.get<Survey>(`${this.url}/${id}/survey`);
  }

  public getCount() {
    return this.http.get<number>(this.url + '/count');
  }

  public getCompletions(id: string) {
    return this.http.get<Completion[]>(`${this.url}/${id}/completions`);
  }

  public getResults(id: string) {
    return this.http.get<SurveyResult[]>(`${this.url}/${id}/results`);
  }

  public getSchedule(id: string) {
    return this.http.get<Schedule>(this.url, id);
  }

  public createSchedule(schedule: Schedule) {
    return this.storage.currentSession$.pipe(
      mergeMap((session) => {
        schedule.assignedBy = session.user;
        return this.http.post<Schedule>(this.url, schedule);
      })
    );
  }

  public editSchedule(id: string, schedule: Schedule) {
    return this.http.put(this.url, id, schedule);
  }

  public deleteSchedule(id: string) {
    return this.http.delete(this.url, id);
  }
}
