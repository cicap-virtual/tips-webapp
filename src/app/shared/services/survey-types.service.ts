import { Injectable } from '@angular/core';

import { SurveyType } from '../models/surveys.model';
import { ConnectionService } from './connection.service';
import { CustomHttpClient } from './custom-http.service';

@Injectable({ providedIn: 'root' })
export class SurveyTypesService {
  private url: string;
  constructor(private http: CustomHttpClient, private conn: ConnectionService) {
    this.url = this.conn.urlAPI + 'surveytypes';
  }

  public getTypes() {
    return this.http.get<SurveyType[]>(this.url);
  }

  public getType(id: string) {
    return this.http.get<SurveyType>(this.url, id);
  }

  public createType(type: SurveyType) {
    return this.http.post<SurveyType>(this.url, type);
  }

  public editType(id: string, type: SurveyType) {
    return this.http.put(this.url, id, type);
  }

  public deleteType(id: string) {
    return this.http.delete(this.url, id);
  }
}
