import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class CustomHttpClient {
  constructor(private http: HttpClient) {}

  createSessionHeader(): HttpHeaders {
    const header = new HttpHeaders();

    header.append('Content-Type', 'application/json');

    return header;
  }

  get<T>(url: string, id?: string) {
    if (id) {
      return this.http.get<T>(`${url}/${id}`, {
        headers: this.createSessionHeader()
      });
    } else {
      return this.http.get<T>(url, { headers: this.createSessionHeader() });
    }
  }

  post<T>(url: string, data: any) {
    return this.http.post<T>(url, data, {
      headers: this.createSessionHeader()
    });
  }

  put(url: string, id: string | number, data: any) {
    return this.http.put(`${url}/${id}`, data, {
      headers: this.createSessionHeader()
    });
  }

  delete(url: string, id: string | number) {
    return this.http.delete(`${url}/${id}`, {
      headers: this.createSessionHeader()
    });
  }
}
