import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Store } from '@ngrx/store';
import { selectIsLogged } from 'src/app/store/selectors';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  constructor(private store$: Store) {}

  canActivate() {
    // return this.auth.isLogged();
    return this.store$.select(selectIsLogged);
  }
}
