import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { firstValueFrom } from 'rxjs';

import {
  Answer,
  Completion,
  MeasureResponse,
  Question,
  Response,
  Survey,
} from '../../models/surveys.model';
import { StorageService } from '../../services/storage.service';

@Component({
  selector: 'app-survey-completion-form',
  templateUrl: './survey-completion-form.component.html',
  styleUrls: ['./survey-completion-form.component.sass'],
})
export class SurveyCompletionFormComponent implements OnInit {
  @Input() survey: Survey;
  @Output() completedForm = new EventEmitter();
  questions: Question[] = [];
  pendingMeasures = false;
  measureQuestions: Question[] = [];
  selected: Answer = {};
  currentIndex = 0;
  instructionsReady = false;
  completed = false;
  completion: Completion = {};
  constructor(private readonly storage: StorageService) {}

  ngOnInit() {
    this.storage.currentProfile$.subscribe({
      next: (profile) => {
        this.completion.surveyId = this.survey.id;
        this.completion.profile = {
          id: profile.id,
          name: profile.name,
          title: profile.position ? profile.position.name : '',
        };
        this.completion.responses = [];
        this.completion.measureResponses = [];
        if (this.survey.type.hasMeasureQuestion) {
          this.pendingMeasures = true;
        }
        this.survey.measures.forEach((m) => {
          const measure: MeasureResponse = {
            measure: { id: m.id, name: m.name },
            answer: null,
            responses: [],
          };

          if (m.mainQuestion?.text) {
            m.mainQuestion.measureQuestion = true;
            measure.mainQuestion = m.mainQuestion;
            this.measureQuestions.push(m.mainQuestion);
          }
          m.questions.forEach((q) => {
            q.measureQuestion = true;
            q.measure = { id: m.id, name: m.name };
            measure.responses.push({ question: q, answer: null });
            this.questions.push(q);
          });
          this.completion.measureResponses.push(measure);
        });
      },
    });
  }

  begin() {
    this.instructionsReady = true;
  }

  measuresCompleted(): boolean {
    return (
      this.completion.measureResponses.filter((x) => x.answer == null)
        .length === 0
    );
  }

  select(answer: Answer) {
    this.selected = answer;
  }

  completeMeasures() {
    this.pendingMeasures = false;
  }

  saveAnswer(measureId?: string) {
    const response: Response = {
      question: this.questions[this.currentIndex],
      answer: this.selected,
    };
    this.completion.responses.push(response);
    this.currentIndex++;
    if (this.currentIndex === this.questions.length) {
      this.completed = true;
      return;
    }
    this.selected = {};
  }

  complete() {
    this.completed = true;
  }

  finish() {
    console.log(this.completion);
    this.completedForm.emit(this.completion);
  }
}
