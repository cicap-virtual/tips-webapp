import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SurveyCompletionFormComponent } from './survey-completion-form.component';

describe('SurveyCompletionFormComponent', () => {
  let component: SurveyCompletionFormComponent;
  let fixture: ComponentFixture<SurveyCompletionFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveyCompletionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveyCompletionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
