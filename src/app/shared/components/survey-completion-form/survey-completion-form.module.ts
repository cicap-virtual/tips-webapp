import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbProgressbarModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoModule } from '@ngneat/transloco';

import { SurveyMeasuresFormModule } from '../survey-measures-form/survey-measures-form.module';
import { SurveyCompletionFormComponent } from './survey-completion-form.component';

@NgModule({
  declarations: [SurveyCompletionFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    NgbProgressbarModule,
    TranslocoModule,
    SurveyMeasuresFormModule
  ],
  exports: [SurveyCompletionFormComponent]
})
export class SurveyCompletionFormModule {}
