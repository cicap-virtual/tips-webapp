import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslocoModule } from '@ngneat/transloco';
import { CalendarModule } from 'angular-calendar';

import { CalendarHeaderModule } from '../calendar-header/calendar-header.module';
import { LoadingModalModule } from '../loading-modal/loading-modal.module';
import { HomeCalendarComponent } from './home-calendar.component';

@NgModule({
  declarations: [HomeCalendarComponent],
  imports: [
    CommonModule,
    LoadingModalModule,
    CalendarHeaderModule,
    TranslocoModule,
    RouterModule,
    CalendarModule
  ],
  exports: [HomeCalendarComponent]
})
export class HomeCalendarModule {}
