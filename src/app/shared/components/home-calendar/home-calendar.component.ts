import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CalendarEvent, CalendarView } from 'angular-calendar';
import { isSameDay, isSameMonth } from 'date-fns';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Schedule } from '../../models/surveys.model';
import { SchedulesService } from '../../services/schedules.service';

@Component({
  selector: 'app-home-calendar',
  templateUrl: './home-calendar.component.html',
  styleUrls: ['./home-calendar.component.sass'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomeCalendarComponent implements OnInit {
  view: CalendarView = CalendarView.Month;

  CalendarView = CalendarView;

  events$: Observable<CalendarEvent[]>;

  viewDate: Date = new Date();

  activeDayIsOpen = false;
  constructor(
    private schedulesService: SchedulesService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getEvents();
  }

  getEvents(): void {
    this.events$ = this.schedulesService.getSchedules().pipe(
      map(res =>
        res.map(schedule => ({
          id: schedule.id,
          title: schedule.description,
          start: new Date(schedule.startDate),
          end: new Date(schedule.dueDate),
          allDay: true
        }))
      )
    );
  }

  dayClicked({
    date,
    events
  }: {
    date: Date;
    events: Array<CalendarEvent<{ schedule: Schedule }>>;
  }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
        this.viewDate = date;
      }
    }
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  eventClicked(event: CalendarEvent) {
    this.router.navigate(['schedules', event.id], {
      relativeTo: this.route.parent
    });
  }
}
