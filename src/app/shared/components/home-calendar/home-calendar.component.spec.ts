import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslocoModule } from '@ngneat/transloco';
import { CalendarDateFormatter, CalendarModule, DateAdapter } from 'angular-calendar';

import { CalendarHeaderModule } from '../calendar-header/calendar-header.module';
import { LoadingModalModule } from '../loading-modal/loading-modal.module';
import { HomeCalendarComponent } from './home-calendar.component';

describe('HomeCalendarComponent', () => {
  let component: HomeCalendarComponent;
  let fixture: ComponentFixture<HomeCalendarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslocoModule,
        CalendarModule,
        CalendarHeaderModule,
        LoadingModalModule,
        HttpClientTestingModule,
        RouterTestingModule
      ],
      declarations: [ HomeCalendarComponent ],
      providers: [ DateAdapter, CalendarDateFormatter ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
