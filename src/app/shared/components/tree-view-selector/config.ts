export class TreeViewConfig {
  public textField = 'name';
  public idField = 'name';
  public childrenField = 'children';
  constructor() {}
}

export class ItemStatus {
  public static NO_SELECTED: ItemStatus = new ItemStatus(
    -1,
    'No selected',
    'far fa-square text-primary'
  );
  public static PARTIAL: ItemStatus = new ItemStatus(
    0,
    'Partially selected',
    'fas fa-minus-square text-primary'
  );

  public static SELECTED: ItemStatus = new ItemStatus(
    1,
    'Selected',
    'fas fa-check-square text-primary'
  );
  private constructor(
    public id: number,
    public name: string,
    public iconClass: string
  ) {}
}
