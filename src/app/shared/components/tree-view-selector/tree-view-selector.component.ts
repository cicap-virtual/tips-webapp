import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';

import { TreeViewConfig } from './config';
import { TreeViewSelectorService } from './tree-view-selector.service';

@Component({
  selector: 'app-tree-view-selector',
  templateUrl: './tree-view-selector.component.html',
  styleUrls: ['./tree-view-selector.component.sass']
})
export class TreeViewSelectorComponent implements OnChanges {
  @Input() config: TreeViewConfig;
  @Input() collection: any[];
  @Input() selection: string[];
  @Output() updateSelection = new EventEmitter();

  selectedIds: string[] = [];

  constructor(private treeViewService: TreeViewSelectorService) {
    if (!this.config) {
      this.config = new TreeViewConfig();
    }

    this.treeViewService.config = this.config;

    if (!this.selection) {
      this.treeViewService.resetList();
    } else {
      this.treeViewService.selectedList = this.selection;
    }
  }

  ngOnChanges(model: SimpleChanges) {
    if (model.collection) {
      if (this.collection) {
        this.treeViewService.collection = this.collection;
      }
    }
  }

  changeSelection() {
    this.updateSelection.emit(this.treeViewService.selectedList);
  }

}
