import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LoadingModalModule } from '../loading-modal/loading-modal.module';
import { TreeViewItemComponent } from './tree-view-item/tree-view-item.component';
import { TreeViewSelectorComponent } from './tree-view-selector.component';
import { TreeViewSelectorService } from './tree-view-selector.service';
@NgModule({
  declarations: [TreeViewSelectorComponent, TreeViewItemComponent],
  imports: [CommonModule, LoadingModalModule],
  exports: [TreeViewSelectorComponent],
  providers: [TreeViewSelectorService]
})
export class TreeViewSelectorModule {}
