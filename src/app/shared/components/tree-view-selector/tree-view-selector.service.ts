/* eslint-disable @typescript-eslint/adjacent-overload-signatures */
import { Injectable } from '@angular/core';
import { TreeViewConfig } from './config';

@Injectable()
export class TreeViewSelectorService {
  private selectedItems: string[];
  private currentConfig: TreeViewConfig;
  private currentCollection: any[];

  get collection() {
    return this.currentCollection;
  }

  get selectedList() {
    return this.selectedItems;
  }
  get config() {
    return this.currentConfig;
  }

  set selectedList(selection: string[]) {
    this.selectedItems = selection;
  }

  set collection(collection: any[]) {
    this.currentCollection = collection;
  }

  set config(config: TreeViewConfig) {
    this.currentConfig = config;
  }

  public resetList() {
    this.selectedItems = [];
  }

  public toogleSelection(id: string) {
    if (this.isSelected(id)) {
      this.removeItem(id);
    } else {
      this.setItem(id);
    }
  }

  public setItem(id: string) {
    if (!this.isSelected(id)) {
      this.selectedItems.push(id);
    }
    this.setChildrens(id, true);
  }

  public removeItem(id: string) {
    if (this.isSelected(id)) {
      this.selectedItems = this.selectedItems.filter(x => x !== id);
    }
    this.setChildrens(id, false);
  }

  public isSelected(id: string): boolean {
    return this.selectedItems.includes(id);
  }

  setChildrens(id: string, selected: boolean) {
    const item = this.findItem(id, this.collection);
    if (item) {
      for (const el of item[this.config.childrenField]) {
        if (selected) {
          this.setItem(el[this.config.idField]);
        } else {
          this.removeItem(el[this.config.idField]);
        }
      }
    }
  }

  findItem(id: any, collection?: any[]) {
    let selected: any;
    for (const item of collection) {
      if (item[this.config.idField] === id) {
        selected = item;
        break;
      } else {
        selected = this.findItem(id, item[this.config.childrenField]);
      }
      if (selected) {
        break;
      }
    }
    return selected;
  }
}
