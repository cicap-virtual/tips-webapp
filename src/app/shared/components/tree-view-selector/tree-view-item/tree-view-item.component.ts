import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TreeViewSelectorService } from '../tree-view-selector.service';

@Component({
  selector: 'app-tree-view-item',
  templateUrl: './tree-view-item.component.html',
  styleUrls: ['./tree-view-item.component.sass']
})
export class TreeViewItemComponent implements OnInit {
  @Input() item: any[];
  @Output() clickItem = new EventEmitter();

  showItems: boolean;

  constructor(public service: TreeViewSelectorService) {}

  ngOnInit() {}

  toggle() {
    this.showItems = !this.showItems;
  }

  toogleSelection(id: string) {
    this.service.toogleSelection(id);
    this.clickItem.emit();
  }

  isSelected() {
    return this.service.isSelected(this.item[this.service.config.idField]);
  }
}
