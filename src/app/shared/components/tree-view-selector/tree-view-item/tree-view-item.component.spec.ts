import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TreeViewItemComponent } from './tree-view-item.component';

describe('TreeViewItemComponent', () => {
  let component: TreeViewItemComponent;
  let fixture: ComponentFixture<TreeViewItemComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeViewItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeViewItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
