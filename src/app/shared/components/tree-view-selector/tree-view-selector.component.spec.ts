import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TreeViewSelectorComponent } from './tree-view-selector.component';

describe('TreeViewSelectorComponent', () => {
  let component: TreeViewSelectorComponent;
  let fixture: ComponentFixture<TreeViewSelectorComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TreeViewSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TreeViewSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
