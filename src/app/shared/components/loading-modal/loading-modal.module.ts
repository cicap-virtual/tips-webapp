import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { LoadingModalComponent } from './loading-modal.component';

@NgModule({
  declarations: [LoadingModalComponent],
  imports: [CommonModule],
  exports: [LoadingModalComponent]
})
export class LoadingModalModule {}
