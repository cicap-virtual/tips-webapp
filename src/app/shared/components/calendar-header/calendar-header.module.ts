import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslocoModule } from '@ngneat/transloco';
import { CalendarModule } from 'angular-calendar';

import { CalendarHeaderComponent } from './calendar-header.component';

@NgModule({
  declarations: [CalendarHeaderComponent],
  imports: [
    CommonModule,
    TranslocoModule,
    FormsModule,
    CalendarModule
  ],
  exports: [CalendarHeaderComponent]
})
export class CalendarHeaderModule {}
