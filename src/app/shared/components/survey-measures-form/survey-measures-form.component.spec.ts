import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveyMeasuresFormComponent } from './survey-measures-form.component';

describe('SurveyMeasuresFormComponent', () => {
  let component: SurveyMeasuresFormComponent;
  let fixture: ComponentFixture<SurveyMeasuresFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SurveyMeasuresFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveyMeasuresFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
