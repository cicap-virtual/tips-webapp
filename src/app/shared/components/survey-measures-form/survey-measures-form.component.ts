import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MeasureResponse } from '../../models/surveys.model';

@Component({
  selector: 'app-survey-measures-form',
  templateUrl: './survey-measures-form.component.html',
  styleUrls: ['./survey-measures-form.component.sass']
})
export class SurveyMeasuresFormComponent implements OnInit {
  @Input() measures: MeasureResponse[];
  @Output() finish = new EventEmitter();
  currentIndex = 0;
  setMeasures = false;
  constructor() {}

  ngOnInit(): void {
    console.log(this.measures);
  }

  finishMeasures() {
    this.setMeasures = true;
  }

  measuresCompleted(): boolean {
    return this.measures.filter(x => x.answer === null).length === 0;
  }

  questionsCompleted(index: number) {
    return (
      this.measures[index].responses.filter(x => x.answer === null).length === 0
    );
  }

  nextMeasure(): void {
    if (this.currentIndex + 1 < this.measures.length) {
      this.currentIndex++;
    } else {
      this.finish.emit();
    }
  }
}
