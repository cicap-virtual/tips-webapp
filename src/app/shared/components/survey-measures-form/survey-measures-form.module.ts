import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { TranslocoModule } from '@ngneat/transloco';

import { SurveyMeasuresFormComponent } from './survey-measures-form.component';

@NgModule({
  imports: [CommonModule, FormsModule, TranslocoModule],
  declarations: [SurveyMeasuresFormComponent],
  exports: [SurveyMeasuresFormComponent]
})
export class SurveyMeasuresFormModule {}
