import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TableOptions } from '@skooltrak/custom-components';
import { Observable } from 'rxjs';

import { Profile } from '../../models/surveys.model';
import { ProfilesService } from '../../services/profiles.service';

@Component({
  selector: 'app-users-assign',
  templateUrl: './users-assign.component.html',
  styleUrls: ['./users-assign.component.sass'],
})
export class UsersAssignComponent implements OnInit {
  @Input() selectedUsers: Profile[];
  @Output() newSelection = new EventEmitter();

  users$: Observable<Profile[]>;
  filteredUsers: Profile[];
  page = 1;
  pageSize = 5;
  searchText = '';
  table = new TableOptions('select');

  constructor(
    private profileService: ProfilesService,
    public activeModal: NgbActiveModal
  ) {}

  ngOnInit() {
    this.table.columns = [
      { name: 'name', title: 'Nombre', filterable: true },
      { name: 'email', title: 'Email', filterable: true },
      { name: 'id', title: 'ID' },
    ];

    this.table.searchable = true;
    this.users$ = this.profileService.getProfiles();
  }

  selectUsers(set: Profile[]) {
    this.selectedUsers = set;
  }

  saveSelection() {
    this.newSelection.emit(this.selectedUsers);
    this.activeModal.close();
  }
}
