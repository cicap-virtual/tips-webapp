import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { UsersAssignComponent } from './users-assign.component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TranslocoModule } from '@ngneat/transloco';

describe('UsersAssignComponent', () => {
  let component: UsersAssignComponent;
  let fixture: ComponentFixture<UsersAssignComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule, TranslocoModule],
        declarations: [UsersAssignComponent],
        providers: [NgbActiveModal]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersAssignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
