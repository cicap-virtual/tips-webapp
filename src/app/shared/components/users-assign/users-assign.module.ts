import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoModule } from '@ngneat/transloco';
import { CustomComponentsModule } from '@skooltrak/custom-components';

import { UsersAssignComponent } from './users-assign.component';

@NgModule({
    declarations: [UsersAssignComponent],
    imports: [
        CommonModule,
        NgbModalModule,
        CustomComponentsModule,
        TranslocoModule
    ],
    exports: [UsersAssignComponent]
})
export class UsersAssignModule {}
