import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ArrayPipe } from './pipes/array.pipe';

@NgModule({
  declarations: [ArrayPipe],
  imports: [CommonModule],
  exports: [ArrayPipe],
})
export class SharedModule {}
