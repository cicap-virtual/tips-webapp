export class RoleEnum {
  public static ADMIN: RoleEnum = new RoleEnum(
    '5cd75dfcf5320f99cd1058fd',
    'Administrador',
    true,
    false
  );
  public static USER: RoleEnum = new RoleEnum(
    '5cd75e0bf5320f99cd1058fe',
    'Usuario',
    false,
    true
  );

  private constructor(
    public id: string,
    public name: string,
    public isAdmin: boolean,
    public isUser: boolean
  ) {}
}
