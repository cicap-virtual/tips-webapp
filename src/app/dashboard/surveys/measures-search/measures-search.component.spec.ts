import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MeasuresSearchComponent } from './measures-search.component';

describe('MeasuresSearchComponent', () => {
  let component: MeasuresSearchComponent;
  let fixture: ComponentFixture<MeasuresSearchComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MeasuresSearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasuresSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
