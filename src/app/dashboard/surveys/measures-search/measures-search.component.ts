import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MeasuresService } from 'src/app/shared/services/measures.service';
import { Observable } from 'rxjs';
import { Measure } from 'src/app/shared/models/surveys.model';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-measures-search',
  templateUrl: './measures-search.component.html',
  styleUrls: ['./measures-search.component.sass'],
})
export class MeasuresSearchComponent implements OnInit {
  @Output() selectingMeasure = new EventEmitter();
  measures$: Observable<Measure[]>;
  selected: Measure;

  constructor(
    private measuresServ: MeasuresService,
    public activeModal: NgbActiveModal
  ) {}

  ngOnInit() {
    this.measures$ = this.measuresServ.getMeasures();
  }

  selectMeasure() {
    this.activeModal.close(this.selected);
  }
}
