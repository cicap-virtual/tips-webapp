import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoService } from '@ngneat/transloco';
import { Observable } from 'rxjs';
import { AnswerSet } from 'src/app/shared/models/surveys.model';
import { AnswersService } from 'src/app/shared/services/answers.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-answers-form',
  templateUrl: './answers-form.component.html',
  styleUrls: ['./answers-form.component.sass'],
})
export class AnswersFormComponent implements OnInit {
  @Input() setId: string;
  @Output() savedAnswers = new EventEmitter();

  answersForm: FormGroup;
  currentSet$: Observable<AnswerSet>;

  constructor(
    public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private answerService: AnswersService,
    private transloco: TranslocoService
  ) {}

  ngOnInit() {
    if (this.setId) {
      this.currentSet$ = this.answerService.getAnswerSet(this.setId);
    }

    this.answersForm = this.fb.group({
      name: ['', [Validators.required]],
      answers: this.fb.array([this.initAnswers()]),
    });
  }

  initAnswers(): FormGroup {
    return this.fb.group({
      text: ['', [Validators.required]],
      sort: [''],
      value: [''],
      reverseValue: [''],
    });
  }

  getNumbers(): number[] {
    const list = [];
    for (
      let i = 1;
      i <= this.answersForm.get('answers')['controls'].length;
      i++
    ) {
      list.push(i);
    }
    return list;
  }

  addAnswer(): void {
    const control = this.answersForm.controls['answers'] as FormArray;
    control.push(this.initAnswers());
  }

  removeAnswer(i: number): void {
    const control = this.answersForm.controls['answers'] as FormArray;
    control.removeAt(i);
  }

  saveSet(model: AnswerSet): void {
    this.answerService.createAnswerSet(model).subscribe({
      next: (response) => {
        swal.fire({
          toast: true,
          icon: 'success',
          text: this.transloco.translate('New answers set added'),
          position: 'top-end',
          showConfirmButton: false,
          timer: 3000,
        });
        this.activeModal.close();
      },
      error: (err) => console.error(err),
    });
  }
}
