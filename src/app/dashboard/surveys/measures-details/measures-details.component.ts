import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MeasuresService } from 'src/app/shared/services/measures.service';
import { mergeMap, Observable } from 'rxjs';
import {
  Measure,
  AnswerSet,
  Question,
} from 'src/app/shared/models/surveys.model';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { AnswersFormComponent } from '../answers-form/answers-form.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AnswersService } from 'src/app/shared/services/answers.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-measures-details',
  templateUrl: './measures-details.component.html',
  styleUrls: ['./measures-details.component.sass'],
})
export class MeasuresDetailsComponent implements OnInit {
  measure: Measure;
  measureForm: FormGroup;
  sets$: Observable<AnswerSet[]>;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private answersService: AnswersService,
    private measuresServ: MeasuresService,
    private modal: NgbModal,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.sets$ = this.answersService.getAnswerSets();
    this.route.params
      .pipe(mergeMap((params) => this.measuresServ.getMeasure(params['id'])))
      .subscribe({
        next: (res) => {
          this.measure = res;
          this.measureForm = this.fb.group({
            id: [this.measure.id],
            name: [
              this.measure.name,
              [Validators.required, Validators.minLength(5)],
            ],
            description: [this.measure.description],
            questions: this.fb.array(this.initExistingQuestion(this.measure)),
          });
        },
        error: (err) => console.error(err),
      });
  }

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  initQuestion(question?: Question): FormGroup {
    return this.fb.group({
      id: [question ? question.id : ''],
      text: [
        question ? question.text : '',
        [Validators.required, Validators.minLength(5)],
      ],
      reverse: [question ? question.reverse : false],
      multiAnswer: [question ? question.multiAnswer : false],
      answerSet: [
        question ? question.answerSet : undefined,
        Validators.required,
      ],
    });
  }

  initExistingQuestion(measure: Measure): FormGroup[] {
    const controls: FormGroup[] = [];
    measure.questions.forEach((question) => {
      controls.push(this.initQuestion(question));
    });
    return controls;
  }

  addQuestion(): void {
    const control = this.measureForm.controls['questions'] as FormArray;
    control.push(this.initQuestion());
  }

  removeQuestion(i: number): void {
    const control = this.measureForm.controls['questions'] as FormArray;
    control.removeAt(i);
  }

  openModal() {
    this.modal
      .open(AnswersFormComponent, {
        size: 'lg',
      })
      .result.then(() => {
        this.sets$ = this.answersService.getAnswerSets();
      });
  }

  saveMeasure() {
    this.measuresServ
      .editMeasure(this.measure.id, this.measureForm.value)
      .subscribe({
        next: () => {
          swal.fire('Dimensión actualizada correctamente', '', 'success');
        },
        error: (err: Error) =>
          swal.fire('Ocurrió un error', 'Intente más tarde', 'error'),
      });
  }
}
