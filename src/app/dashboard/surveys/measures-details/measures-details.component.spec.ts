import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { MeasuresDetailsComponent } from './measures-details.component';

describe('MeasuresDetailsComponent', () => {
  let component: MeasuresDetailsComponent;
  let fixture: ComponentFixture<MeasuresDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MeasuresDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeasuresDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
