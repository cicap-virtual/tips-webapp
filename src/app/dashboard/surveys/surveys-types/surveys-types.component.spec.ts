import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TranslocoModule } from '@ngneat/transloco';

import { SurveysTypesComponent } from './surveys-types.component';

describe('SurveysTypesComponent', () => {
  let component: SurveysTypesComponent;
  let fixture: ComponentFixture<SurveysTypesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslocoModule,
        HttpClientTestingModule
      ],
      declarations: [ SurveysTypesComponent ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveysTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
