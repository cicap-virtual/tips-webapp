import { Component, OnInit } from '@angular/core';
import { TableOptions } from '@skooltrak/custom-components';
import { Observable } from 'rxjs';
import { SurveyType } from 'src/app/shared/models/surveys.model';
import { SurveyTypesService } from 'src/app/shared/services/survey-types.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-surveys-types',
  templateUrl: './surveys-types.component.html',
  styleUrls: ['./surveys-types.component.sass'],
})
export class SurveysTypesComponent implements OnInit {
  table = new TableOptions();
  types$: Observable<SurveyType[]>;

  constructor(private typesService: SurveyTypesService) {}

  ngOnInit() {
    this.table.columns = [
      {
        name: 'name',
        title: 'Nombre',
        sortable: true,
        filterable: true,
        required: true,
      },
      {
        name: 'hasRadar',
        title: 'Gráfica de radar',
        type: 'boolean',
        required: true,
      },
      {
        name: 'hasBar',
        title: 'Gráfica de barras',
        type: 'boolean',
        required: true,
      },
      {
        name: 'hasMeasureQuestion',
        title: 'Pregunta de dimensión',
        type: 'boolean',
        required: true,
        hidden: true,
      },
      {
        name: 'wheelCharts',
        title: 'Gráficas de rueda',
        type: 'boolean',
        required: true,
      },
      {
        name: 'prefix',
        title: 'Prefijo',
        hidden: true,
      },
      {
        name: 'isRandom',
        title: 'Aleatorias',
        hidden: true,
        type: 'boolean',
      },
      {
        name: 'measureName',
        title: 'Nombre de dimensión',
        hidden: true,
      },
      {
        name: 'subMeasureName',
        title: 'Nombre de subdimensión',
        hidden: true,
      },
      {
        name: 'visibleMeasures',
        title: 'Dimensiones visibles',
        type: 'boolean',
        hidden: true,
      },
      {
        name: 'instructions',
        title: 'Instrucciones',
        hidden: true,
        type: 'text',
      },
    ];

    this.table.accessCode = 'categories';
    this.types$ = this.typesService.getTypes();
  }

  createType(type: SurveyType): void {
    this.typesService.createType(type).subscribe({
      next: (res) => {
        swal.fire('Categoría creada exitosamente', res.name, 'success');
        this.types$ = this.typesService.getTypes();
      },
      error: (err: Error) =>
        swal.fire('Ocurrió un error', 'Intente más tarde', 'error'),
    });
  }

  editType(type: SurveyType): void {
    this.typesService.editType(type.id, type).subscribe({
      next: () => {
        swal.fire('Categoría editada exitosamente', '', 'success');
        this.types$ = this.typesService.getTypes();
      },
      error: (err: Error) =>
        swal.fire('Ocurrió un error', 'Intente más tarde', 'error'),
    });
  }

  deleteType(id: string): void {
    this.typesService.deleteType(id).subscribe({
      next: () => {
        swal.fire('Categoría eliminada exitosamente', '', 'info');
        this.types$ = this.typesService.getTypes();
      },
      error: (err: Error) =>
        swal.fire('Ocurrió un error', 'Intente más tarde', 'error'),
    });
  }
}
