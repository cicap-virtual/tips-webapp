import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeactivateGuard } from 'src/app/shared/services/survey-form.guard';

import { MeasuresDetailsComponent } from './measures-details/measures-details.component';
import { SurveyDetailsComponent } from './survey-details/survey-details.component';
import { SurveyEditComponent } from './survey-edit/survey-edit.component';
import { SurveyNewComponent } from './survey-new/survey-new.component';
import { SurveysHomeComponent } from './surveys-home/surveys-home.component';
import { SurveysComponent } from './surveys.component';

const routes: Routes = [
  {
    path: '',
    component: SurveysComponent,
    children: [
      { path: '', component: SurveysHomeComponent },
      {
        path: 'new',
        component: SurveyNewComponent,
        canDeactivate: [DeactivateGuard]
      },
      { path: 'measures', redirectTo: '', pathMatch: 'full'},
      { path: ':id/edit', component: SurveyEditComponent },
      { path: ':id', component: SurveyDetailsComponent },
      { path: 'measures/:id', component: MeasuresDetailsComponent },
      { path: '**', redirectTo: '', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class SurveysRoutingModule {}
