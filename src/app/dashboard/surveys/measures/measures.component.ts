import { Component, OnInit } from '@angular/core';
import { TableOptions } from '@skooltrak/custom-components';
import { Observable } from 'rxjs';
import { Measure } from 'src/app/shared/models/surveys.model';
import { MeasuresService } from 'src/app/shared/services/measures.service';

@Component({
  selector: 'app-measures',
  templateUrl: './measures.component.html',
  styleUrls: ['./measures.component.sass'],
})
export class MeasuresComponent implements OnInit {
  measures$: Observable<Measure[]>;
  table = new TableOptions();

  constructor(private measuresServ: MeasuresService) {}

  ngOnInit() {
    this.table.columns = [
      { name: 'name', title: 'Nombre' },
      { name: 'description', title: 'Descripción' },
    ];

    this.table.searchable = true;
    this.measures$ = this.measuresServ.getMeasures();
    this.table.detailsURL = ['measures'];
  }
}
