import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslocoService } from '@ngneat/transloco';
import { mergeMap } from 'rxjs';
import { Survey } from 'src/app/shared/models/surveys.model';
import { SurveysService } from 'src/app/shared/services/surveys.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-survey-edit',
  templateUrl: './survey-edit.component.html',
  styleUrls: ['./survey-edit.component.sass'],
})
export class SurveyEditComponent implements OnInit {
  survey: Survey;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private transloco: TranslocoService,
    private surveyService: SurveysService
  ) {}

  ngOnInit() {
    this.getSurvey();
  }

  getSurvey() {
    this.route.params
      .pipe(mergeMap((params) => this.surveyService.getSurvey(params['id'])))
      .subscribe({ next: (survey) => (this.survey = survey) });
  }

  updateSurvey(survey: Survey) {
    this.surveyService.editSurvey(survey.id, survey).subscribe({
      next: () => {
        swal.fire(
          survey.title,
          this.transloco.translate('Survey updated successfully'),
          'success'
        );
        this.router.navigate([`./${survey.id}`], {
          relativeTo: this.route.parent,
        });
      },
      error: (err: Error) =>
        swal.fire(
          this.transloco.translate('Something went wrong'),
          this.transloco.translate(err.message),
          'error'
        ),
    });
  }
}
