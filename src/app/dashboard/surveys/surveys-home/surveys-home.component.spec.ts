import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoModule } from '@ngneat/transloco';

import { SurveysListsComponent } from '../surveys-lists/surveys-lists.component';
import { SurveysTypesComponent } from '../surveys-types/surveys-types.component';
import { SurveysHomeComponent } from './surveys-home.component';

describe('SurveysHomeComponent', () => {
  let component: SurveysHomeComponent;
  let fixture: ComponentFixture<SurveysHomeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslocoModule,
        NgbNavModule,
        HttpClientTestingModule
      ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
      declarations: [
        SurveysHomeComponent,
        SurveysTypesComponent,
        SurveysListsComponent
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveysHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
