import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModalModule, NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoModule } from '@ngneat/transloco';
import { CustomComponentsModule } from '@skooltrak/custom-components';
import { LoadingModalModule } from 'src/app/shared/components/loading-modal/loading-modal.module';

import { AnswersFormComponent } from './answers-form/answers-form.component';
import { MeasuresDetailsComponent } from './measures-details/measures-details.component';
import { MeasuresFormComponent } from './measures-form/measures-form.component';
import { MeasuresSearchComponent } from './measures-search/measures-search.component';
import { MeasuresComponent } from './measures/measures.component';
import { QuestionsComponent } from './questions/questions.component';
import { SurveyDetailsComponent } from './survey-details/survey-details.component';
import { SurveyEditComponent } from './survey-edit/survey-edit.component';
import { SurveyFormComponent } from './survey-form/survey-form.component';
import { SurveyNewComponent } from './survey-new/survey-new.component';
import { SurveysHomeComponent } from './surveys-home/surveys-home.component';
import { SurveysListsComponent } from './surveys-lists/surveys-lists.component';
import { SurveysTempComponent } from './surveys-temp/surveys-temp.component';
import { SurveysTypesComponent } from './surveys-types/surveys-types.component';
import { SurveysComponent } from './surveys.component';
import { SurveysRoutingModule } from './surveys.routes';
import { QuestionComponent } from './question/question.component';
import { AnswerSetsComponent } from './answer-sets/answer-sets.component';
import { CategoriesComponent } from './categories/categories.component';

@NgModule({
  declarations: [
    SurveysComponent,
    SurveyFormComponent,
    SurveyNewComponent,
    SurveyEditComponent,
    SurveyDetailsComponent,
    SurveysListsComponent,
    MeasuresFormComponent,
    AnswersFormComponent,
    SurveysHomeComponent,
    SurveysTypesComponent,
    QuestionsComponent,
    SurveysTempComponent,
    MeasuresComponent,
    MeasuresDetailsComponent,
    MeasuresSearchComponent,
    QuestionComponent,
    AnswerSetsComponent,
    CategoriesComponent
  ],
  imports: [
    CommonModule,
    SurveysRoutingModule,
    TranslocoModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModalModule,
    NgbNavModule,
    CustomComponentsModule,
    LoadingModalModule
  ],
  providers: [DatePipe]
})
export class SurveysModule {}
