import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { mergeMap, Observable } from 'rxjs';
import { Survey } from 'src/app/shared/models/surveys.model';
import { SurveysService } from 'src/app/shared/services/surveys.service';

@Component({
  selector: 'app-survey-details',
  templateUrl: './survey-details.component.html',
  styleUrls: ['./survey-details.component.sass'],
})
export class SurveyDetailsComponent implements OnInit {
  survey$: Observable<Survey>;
  constructor(
    private route: ActivatedRoute,
    private surveysServ: SurveysService
  ) {}

  ngOnInit() {
    this.survey$ = this.route.params.pipe(
      mergeMap((params) => this.surveysServ.getSurvey(params['id']))
    );
  }
}
