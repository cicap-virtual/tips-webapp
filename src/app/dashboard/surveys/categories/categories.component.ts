import { Component, OnInit } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { TableOptions } from '@skooltrak/custom-components';
import { Observable } from 'rxjs';
import { SurveyCategory } from 'src/app/shared/models/surveys.model';
import { CategoriesService } from 'src/app/shared/services/categories.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.sass'],
})
export class CategoriesComponent implements OnInit {
  categories$: Observable<SurveyCategory[]>;
  table = new TableOptions();
  constructor(
    private readonly categoriesService: CategoriesService,
    private readonly transloco: TranslocoService
  ) {}

  ngOnInit(): void {
    this.table.columns = [
      { name: 'name', title: this.transloco.translate('Name'), required: true },
    ];
    this.categories$ = this.categoriesService.getAll();
  }
}
