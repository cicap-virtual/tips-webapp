import { Component, OnInit } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { TableOptions } from '@skooltrak/custom-components';
import { Observable } from 'rxjs';
import { Question } from 'src/app/shared/models/surveys.model';
import { AnswersService } from 'src/app/shared/services/answers.service';
import { QuestionsService } from 'src/app/shared/services/questions.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.sass'],
})
export class QuestionsComponent implements OnInit {
  answers$ = this.answerServ.getAnswerSets();
  questions$: Observable<Question[]>;

  options = new TableOptions();

  constructor(
    private readonly questionsServ: QuestionsService,
    private readonly answerServ: AnswersService,
    private readonly transloco: TranslocoService
  ) {}

  ngOnInit() {
    this.options.columns = [
      { name: 'text', title: 'Texto', filterable: true, required: true },
      {
        name: 'reverse',
        title: 'Valor inverso',
        type: 'boolean',
        required: true,
      },
      {
        name: 'multiAnswer',
        title: 'Múltiple',
        type: 'boolean',
        required: true,
      },
      {
        name: 'answerSet',
        title: 'Respuestas',
        type: 'object',
        objectText: 'name',
        listDisplay: 'name',
        objectColumn: 'answerSet.name',
        asyncList: this.answers$,
        required: true,
      },
    ];

    this.options.searchable = true;
    this.options.accessCode = 'questions';

    this.questions$ = this.questionsServ.getQuestions();
  }

  createQuestion(question: Question) {
    this.questionsServ.createQuestion(question).subscribe({
      next: (res) => {
        swal.fire(
          this.transloco.translate('createdQuestion'),
          this.transloco.translate('success'),
          'success'
        );
        this.questions$ = this.questionsServ.getQuestions();
      },
      error: (err) => console.error(err),
    });
  }

  editQuestion(question: Question) {
    this.questionsServ.editQuestion(question.id, question).subscribe({
      next: (res) => {
        swal.fire(
          this.transloco.translate('updatedQuestion'),
          this.transloco.translate('success'),
          'success'
        );
        this.questions$ = this.questionsServ.getQuestions();
      },
      error: (err) => console.error(err),
    });
  }

  deleteQuestions(id: string) {
    this.questionsServ.deleteQuestion(id).subscribe({
      next: (res) => {
        swal.fire(this.transloco.translate('deletedQuestion'), '', 'info');
      },
      error: (err) => console.error(err),
    });
  }
}
