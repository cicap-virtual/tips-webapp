import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { AnswerSet } from 'src/app/shared/models/surveys.model';

import { AnswersFormComponent } from '../answers-form/answers-form.component';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.sass'],
})
export class QuestionComponent implements OnInit {
  @Input() question: FormGroup;
  @Input() sets$: Observable<AnswerSet[]>;
  @Output() updateAnswers = new EventEmitter();

  constructor(private modal: NgbModal) {}

  ngOnInit(): void {
    console.log('question: ', this.question);
  }

  openModal() {
    this.modal
      .open(AnswersFormComponent, {
        size: 'lg',
        backdrop: 'static',
        keyboard: false,
      })
      .result.then(
        () => {
          this.updateAnswers.emit();
        },
        () => {}
      );
  }

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
}
