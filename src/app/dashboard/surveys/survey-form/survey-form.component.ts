import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoService } from '@ngneat/transloco';
import { Observable } from 'rxjs';
import {
  AnswerSet,
  Measure,
  Question,
  Survey,
  SurveyType,
} from 'src/app/shared/models/surveys.model';
import { AnswersService } from 'src/app/shared/services/answers.service';
import { SurveyTypesService } from 'src/app/shared/services/survey-types.service';

import { MeasuresSearchComponent } from '../measures-search/measures-search.component';

@Component({
  selector: 'app-survey-form',
  templateUrl: './survey-form.component.html',
  styleUrls: ['./survey-form.component.sass'],
})
export class SurveyFormComponent implements OnInit {
  @Input() survey: Survey;
  @Output() saveSurvey = new EventEmitter();
  public surveyForm: FormGroup;
  types$: Observable<SurveyType[]>;
  answerSets$: Observable<AnswerSet[]>;
  activeTab: number;

  constructor(
    private fb: FormBuilder,
    private typesService: SurveyTypesService,
    private answersService: AnswersService,
    private transloco: TranslocoService,
    private modal: NgbModal
  ) {}

  ngOnInit() {
    this.types$ = this.typesService.getTypes();
    this.surveyForm = this.fb.group({
      id: [this.survey ? this.survey.id : ''],
      title: [
        this.survey ? this.survey.title : '',
        [Validators.required, Validators.minLength(5)],
      ],
      type: [this.survey ? this.survey.type : undefined, [Validators.required]],
      description: [this.survey ? this.survey.description : ''],
      creditsCost: [this.survey ? this.survey.creditsCost : 0],
      public: [this.survey ? this.survey.public : false],
      measures: this.survey
        ? this.fb.array(this.initExistingMeasure())
        : this.fb.array([this.initMeasure()]),
      createDate: [this.survey ? this.survey.createdDate : ''],
      createdBy: [this.survey ? this.survey.createdBy : undefined],
      final: [this.survey ? this.survey.final : false],
    });

    if (!this.survey) {
      this.surveyForm.controls['type'].setValue(undefined);
    } else {
      this.surveyForm.controls['type'].setValue(this.survey.type);
    }
    this.updateSets();
    this.activeTab = 0;
  }

  updateSets() {
    this.answerSets$ = this.answersService.getAnswerSets();
  }

  measureName(): string {
    if (
      this.surveyForm.controls['type'].value &&
      this.surveyForm.controls['type'].value.measureName
    ) {
      return this.surveyForm.controls['type'].value.measureName;
    } else {
      return this.transloco.translate('measure');
    }
  }

  initMeasure(measure?: Measure): FormGroup {
    return this.fb.group({
      id: [measure ? measure.id : ''],
      name: [
        measure ? measure.name : '',
        [Validators.required, Validators.minLength(5)],
      ],
      subMeasures: [[]],
      mainQuestion: measure
        ? this.survey.type.hasMeasureQuestion
          ? this.initQuestion(measure.mainQuestion)
          : null
        : this.initQuestion(),
      weighting: [
        measure ? measure.weighting : 10,
        [Validators.required, Validators.min(1)],
      ],
      description: [measure ? measure.description : ''],
      questions: measure
        ? this.fb.array(this.initExistingQuestion(measure))
        : this.fb.array([this.initQuestion()]),
    });
  }

  initExistingMeasure(): FormGroup[] {
    const controls: FormGroup[] = [];
    this.survey.measures.forEach((item) => {
      controls.push(this.initMeasure(item));
    });
    return controls;
  }

  addMeasure(): void {
    const control = this.surveyForm.controls['measures'] as FormArray;
    control.push(this.initMeasure());
    this.activeTab = control.controls.length - 1;
  }

  removeMeasure(i: number): void {
    const control = this.surveyForm.controls['measures'] as FormArray;
    control.removeAt(i);
    this.activeTab = 0;
  }

  initQuestion(question?: Question): FormGroup {
    return this.fb.group({
      id: [question ? question.id : ''],
      title: [question ? question.title : ''],
      text: [
        question ? question.text : '',
        [Validators.required, Validators.minLength(5)],
      ],
      weighting: [
        question ? question.weighting : 10,
        [Validators.required, Validators.min(1)],
      ],
      reverse: [question ? question.reverse : false],
      multiAnswer: [question ? question.multiAnswer : false],
      answerSet: [
        question ? question.answerSet : undefined,
        Validators.required,
      ],
    });
  }

  initExistingQuestion(measure: Measure): FormGroup[] {
    const controls: FormGroup[] = [];
    measure.questions.forEach((question) => {
      controls.push(this.initQuestion(question));
    });
    return controls;
  }

  addQuestion(array: FormArray): void {
    const control = array.controls['questions'];
    control.push(this.initQuestion());
  }

  removeQuestion(measureId: number, i: number): void {
    const measure = this.surveyForm.controls['measures']['controls'][
      measureId
    ] as FormGroup;
    const control = measure.controls['questions'] as FormArray;
    control.removeAt(i);
  }

  hasMeasureQuestion(): boolean {
    return this.surveyForm.get('type').value?.hasMeasureQuestion;
  }

  updateSurveyType() {
    const measures = this.surveyForm.controls['measures'] as FormArray;
    if (this.hasMeasureQuestion()) {
      measures.controls.forEach((measure) => {
        console.log('measure:', measure);
        measure = measure as FormGroup;
        if (!measure.get('mainQuestion')) {
          (measure as FormGroup).addControl(
            'mainQuestion',
            this.initQuestion()
          );
          console.log('measure:', measure);
        }
      });
    } else {
      measures.controls.forEach((measure) => {
        measure = measure as FormGroup;
        if (measure.get('mainQuestion')) {
          (measure as FormGroup).removeControl('mainQuestion');
        }
      });
    }
    console.log('form:', this.surveyForm);
  }

  save(): void {
    this.surveyForm.markAllAsTouched();
    if (this.surveyForm.valid) {
      this.saveSurvey.emit(this.surveyForm.value);
    }
  }

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }

  searchMeasure(i: number) {
    this.modal
      .open(MeasuresSearchComponent, { backdrop: 'static', keyboard: false })
      .result.then((result: Measure) => {
        this.surveyForm.get('measures')['controls'][i].patchValue(result);
        this.surveyForm['controls'].measures['controls'][i] =
          this.initMeasure(result);
        this.surveyForm.get('measures')['controls'][i].patchValue(result);
      });
  }
}
