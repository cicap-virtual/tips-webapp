import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { TranslocoModule } from '@ngneat/transloco';

import { SurveysListsComponent } from './surveys-lists.component';

describe('SurveysListsComponent', () => {
  let component: SurveysListsComponent;
  let fixture: ComponentFixture<SurveysListsComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          TranslocoModule,
          HttpClientTestingModule
        ],
        declarations: [SurveysListsComponent]
      }).compileComponents();
    })
  );

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveysListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
