import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { TableOptions } from '@skooltrak/custom-components';
import { Observable } from 'rxjs';
import { Survey } from 'src/app/shared/models/surveys.model';
import { SurveysService } from 'src/app/shared/services/surveys.service';
import { selectCurrentRole } from 'src/app/store/selectors';

@Component({
  selector: 'app-surveys-lists',
  templateUrl: './surveys-lists.component.html',
  styleUrls: ['./surveys-lists.component.sass'],
})
export class SurveysListsComponent implements OnInit {
  surveys$: Observable<Survey[]>;
  options = new TableOptions();
  role$ = this.store$.select(selectCurrentRole);
  constructor(private surveyService: SurveysService, private store$: Store) {}

  ngOnInit() {
    this.role$.subscribe({
      next: (role) => {
        const access = role.permissions.find(
          (x) => x.access.code === 'surveys'
        );
        if (!!access) {
          this.options.permissions.create = access.create;
          this.options.permissions.edit = access.edit;
          this.options.permissions.read = access.read;
          this.options.permissions.delete = access.delete;
        } else {
          this.options.permissions.create = false;
          this.options.permissions.edit = false;
          this.options.permissions.read = false;
          this.options.permissions.delete = false;
        }
      },
    });
    this.options.accessCode = 'surveys';
    this.options.columns = [
      {
        name: 'title',
        title: 'Nombre',
        sortable: true,
        filterable: true,
      },
      {
        name: 'type',
        title: 'Categoría',
        type: 'object',
        objectColumn: 'type.name',
        lookup: true,
      },
      { name: 'creditsCost', title: 'Costo/créditos', type: 'number' },
      {
        name: 'createdDate',
        title: 'Fec. Creación',
        type: 'datetime',
      },
      {
        name: 'modificateDate',
        title: 'Fec. Modificación',
        type: 'datetime',
      },
      {
        name: 'createdBy',
        title: 'Creado por',
        type: 'object',
        objectColumn: 'createdBy.userName',
        lookup: true,
      },
    ];

    this.options.newURL = ['new'];
    this.options.accessCode = 'surveys';
    this.options.searchable = true;
    this.options.detailsURL = [];
    this.options.lookup = true;
    this.surveys$ = this.surveyService.getSurveys();
  }
}
