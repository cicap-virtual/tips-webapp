import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AnswerSet } from 'src/app/shared/models/surveys.model';
import { AnswersService } from 'src/app/shared/services/answers.service';

@Component({
  selector: 'app-answer-sets',
  templateUrl: './answer-sets.component.html',
  styleUrls: ['./answer-sets.component.sass'],
})
export class AnswerSetsComponent implements OnInit {
  sets$: Observable<AnswerSet[]>;
  constructor(private answersService: AnswersService) {}

  ngOnInit(): void {
    this.sets$ = this.answersService.getAnswerSets();
  }
}
