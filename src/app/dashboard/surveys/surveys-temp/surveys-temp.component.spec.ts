import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SurveysTempComponent } from './surveys-temp.component';

describe('SurveysTempComponent', () => {
  let component: SurveysTempComponent;
  let fixture: ComponentFixture<SurveysTempComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveysTempComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveysTempComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
