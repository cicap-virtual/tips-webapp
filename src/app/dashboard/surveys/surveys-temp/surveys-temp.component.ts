import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Survey } from 'src/app/shared/models/surveys.model';
import { SurveysService } from 'src/app/shared/services/surveys.service';
import { UtilService } from 'src/app/shared/services/util.service';

@Component({
  selector: 'app-surveys-temp',
  templateUrl: './surveys-temp.component.html',
  styleUrls: ['./surveys-temp.component.sass'],
})
export class SurveysTempComponent implements OnInit {
  @Input() temps: Survey[];
  @Output() selection = new EventEmitter();
  constructor(
    public activeModal: NgbActiveModal,
    private readonly surveyServ: SurveysService,
    private readonly util: UtilService
  ) {}

  ngOnInit() {}

  delete(id: string) {
    this.surveyServ.deleteSurvey(id).subscribe({
      next: (res) => {
        this.temps = this.util.removeById(this.temps, id);
      },
      error: (err) => console.error(err),
    });
  }

  select(id: string) {
    this.activeModal.close(id);
  }
}
