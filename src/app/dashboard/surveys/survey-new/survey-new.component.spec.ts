import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslocoModule } from '@ngneat/transloco';

import { MeasuresFormComponent } from '../measures-form/measures-form.component';
import { SurveyFormComponent } from '../survey-form/survey-form.component';
import { SurveyNewComponent } from './survey-new.component';

describe('SurveyNewComponent', () => {
  let component: SurveyNewComponent;
  let fixture: ComponentFixture<SurveyNewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientTestingModule,
        ReactiveFormsModule,
        TranslocoModule
      ],
      declarations: [
        SurveyNewComponent,
        SurveyFormComponent,
        MeasuresFormComponent
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveyNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
