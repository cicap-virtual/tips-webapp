import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoService } from '@ngneat/transloco';
import { Survey } from 'src/app/shared/models/surveys.model';
import { SurveysService } from 'src/app/shared/services/surveys.service';
import swal from 'sweetalert2';

import { SurveyFormComponent } from '../survey-form/survey-form.component';
import { SurveysTempComponent } from '../surveys-temp/surveys-temp.component';

@Component({
  selector: 'app-survey-new',
  templateUrl: './survey-new.component.html',
  styleUrls: ['./survey-new.component.sass'],
})
export class SurveyNewComponent implements OnInit {
  @ViewChild(SurveyFormComponent) form: SurveyFormComponent;

  isFinal = false;
  ready = false;
  temp: Survey;

  constructor(
    private surveyService: SurveysService,
    private router: Router,
    private route: ActivatedRoute,
    private transloco: TranslocoService,
    private modal: NgbModal
  ) {}

  ngOnInit() {
    this.surveyService.getTemp().subscribe({
      next: (res) => {
        if (res.length > 0) {
          this.getTempSurvey(res);
        } else {
          this.ready = true;
        }
      },
    });
  }

  getTempSurvey(surveys: Survey[]) {
    const modalRef = this.modal.open(SurveysTempComponent);
    modalRef.componentInstance.temps = surveys;
    modalRef.result.then(
      (result: string) =>
        this.surveyService.getSurvey(result).subscribe({
          next: (resp) => {
            this.temp = resp;
            this.ready = true;
          },
        }),
      () => {
        this.ready = true;
      }
    );
  }

  createSurvey(survey: Survey) {
    this.isFinal = true;
    this.surveyService.createSurvey(survey).subscribe({
      next: (res) => {
        swal.fire(
          survey.title,
          this.transloco.translate('Survey created successfully'),
          'success'
        );
        this.router.navigate(['./', res.id], {
          relativeTo: this.route.parent,
        });
      },
      error: (err: Error) => {
        swal.fire(
          this.transloco.translate('Something went wrong'),
          this.transloco.translate(err.message),
          'error'
        );
      },
    });
  }

  canDeactivate(): Promise<boolean> | boolean {
    if (
      this.form &&
      this.form.surveyForm.dirty &&
      this.form.surveyForm.value.title &&
      !this.isFinal
    ) {
      return swal
        .fire({
          title: this.transloco.translate('wantExit'),
          text: this.transloco.translate('continueSurvey'),
          icon: 'question',
          showCancelButton: true,
          confirmButtonColor: '#bfbfbf',
          cancelButtonColor: '#7f5e86',
          confirmButtonText: this.transloco.translate('quit'),
          cancelButtonText: this.transloco.translate('continue'),
        })
        .then((result) => {
          if (result.value) {
            this.surveyService.saveTemp(this.form.surveyForm.value).subscribe({
              next: (res) => {
                console.log(res);
              },
              error: (err: Error) => console.error(err),
            });
            return true;
          } else {
            return false;
          }
        });
    } else {
      return true;
    }
  }
}
