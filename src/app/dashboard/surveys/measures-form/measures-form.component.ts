import { Component, EventEmitter, Input, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { AnswerSet, SurveyType } from 'src/app/shared/models/surveys.model';

import { AnswersFormComponent } from '../answers-form/answers-form.component';

@Component({
  selector: 'app-measures-form',
  templateUrl: './measures-form.component.html',
  styleUrls: ['./measures-form.component.sass'],
})
export class MeasuresFormComponent {
  // eslint-disable-next-line @angular-eslint/no-input-rename
  @Input('group')
  public measuresForm: FormGroup;
  @Input() type: SurveyType;
  @Input() sets$: Observable<AnswerSet[]>;
  @Input() hasMeasureQuestion: boolean;
  @Output() addQuestion = new EventEmitter();
  @Output() removeQuestion = new EventEmitter();
  @Output() updateAnswers = new EventEmitter();

  constructor(private modal: NgbModal) {}

  public addClick() {
    this.addQuestion.emit(this.measuresForm);
  }

  public removeClick(id: number) {
    this.removeQuestion.emit(id);
  }

  getChildForm(key: string) {
    return this.measuresForm.get(key) as FormGroup;
  }

  openModal() {
    this.modal
      .open(AnswersFormComponent, {
        size: 'lg',
        backdrop: 'static',
        keyboard: false,
      })
      .result.then(
        () => {
          this.updateAnswers.emit();
        },
        () => {}
      );
  }

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
}
