import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, inject, TestBed, waitForAsync } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoModule } from '@ngneat/transloco';

import { MeasuresFormComponent } from './measures-form.component';

describe('MeasuresFormComponent', () => {
  let component: MeasuresFormComponent;
  let fixture: ComponentFixture<MeasuresFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        ReactiveFormsModule,
        NgbModalModule,
        TranslocoModule,
        HttpClientTestingModule
      ],
      declarations: [MeasuresFormComponent]
    }).compileComponents();
  }));

  beforeEach(inject([FormBuilder], (fb: FormBuilder) => {
    fixture = TestBed.createComponent(MeasuresFormComponent);
    component = fixture.componentInstance;
    component.measuresForm = fb.group({
      name: [''],
      description: [''],
      questions: ['']
    });
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
