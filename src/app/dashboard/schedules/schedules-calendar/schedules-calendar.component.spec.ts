import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslocoModule } from '@ngneat/transloco';
import { CalendarDateFormatter, CalendarModule, DateAdapter } from 'angular-calendar';
import { CalendarHeaderModule } from 'src/app/shared/components/calendar-header/calendar-header.module';
import { LoadingModalModule } from 'src/app/shared/components/loading-modal/loading-modal.module';

import { SchedulesCalendarComponent } from './schedules-calendar.component';

describe('SchedulesCalendarComponent', () => {
  let component: SchedulesCalendarComponent;
  let fixture: ComponentFixture<SchedulesCalendarComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        TranslocoModule,
        CalendarModule,
        CalendarHeaderModule,
        LoadingModalModule
      ],
      declarations: [SchedulesCalendarComponent],
      providers: [DateAdapter, CalendarDateFormatter]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulesCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
