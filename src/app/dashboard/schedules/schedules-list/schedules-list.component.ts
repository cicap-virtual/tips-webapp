import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SchedulesService } from 'src/app/shared/services/schedules.service';
import { Schedule } from 'src/app/shared/models/surveys.model';

@Component({
  selector: 'app-schedules-list',
  templateUrl: './schedules-list.component.html',
  styleUrls: ['./schedules-list.component.sass']
})
export class SchedulesListComponent implements OnInit {
  schedules$: Observable<Schedule[]>;
  today = new Date();
  showInactive = false;
  constructor(private schedulesService: SchedulesService) {}
  ngOnInit() {
    this.schedules$ = this.schedulesService.getSchedules();
  }

  isActive(schedule: Schedule): boolean {
    return schedule.status === 'Active';
  }
}
