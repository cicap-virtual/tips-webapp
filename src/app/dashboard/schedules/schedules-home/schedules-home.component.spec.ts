import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoModule } from '@ngneat/transloco';
import { CalendarDateFormatter, CalendarModule, DateAdapter } from 'angular-calendar';
import { CalendarHeaderModule } from 'src/app/shared/components/calendar-header/calendar-header.module';
import { LoadingModalModule } from 'src/app/shared/components/loading-modal/loading-modal.module';

import { SchedulesCalendarComponent } from '../schedules-calendar/schedules-calendar.component';
import { SchedulesListComponent } from '../schedules-list/schedules-list.component';
import { SchedulesHomeComponent } from './schedules-home.component';

describe('SchedulesHomeComponent', () => {
  let component: SchedulesHomeComponent;
  let fixture: ComponentFixture<SchedulesHomeComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        TranslocoModule,
        NgbNavModule,
        RouterTestingModule,
        CalendarModule,
        CalendarHeaderModule,
        LoadingModalModule,
        HttpClientTestingModule
      ],
      declarations: [
        SchedulesHomeComponent,
        SchedulesCalendarComponent,
        SchedulesListComponent
      ],
      providers: [DateAdapter, CalendarDateFormatter]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulesHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
