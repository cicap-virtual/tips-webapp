import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { mergeMap } from 'rxjs';
import { Schedule } from 'src/app/shared/models/surveys.model';
import { SchedulesService } from 'src/app/shared/services/schedules.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-schedules-edit',
  templateUrl: './schedules-edit.component.html',
  styleUrls: ['./schedules-edit.component.sass'],
})
export class SchedulesEditComponent implements OnInit {
  schedule: Schedule;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly schedulesServ: SchedulesService
  ) {}

  ngOnInit() {
    this.getSchedule();
  }

  getSchedule() {
    this.route.params
      .pipe(mergeMap((params) => this.schedulesServ.getSchedule(params['id'])))
      .subscribe({
        next: (res) => {
          this.schedule = res;
        },
        error: (err) => console.error(err),
      });
  }

  editSchedule(schedule: Schedule) {
    this.schedulesServ.editSchedule(schedule.id, schedule).subscribe({
      next: () => {
        swal.fire('Programación actualizada exitosamente', '', 'success');
        this.router.navigate([`./${schedule.id}`], {
          relativeTo: this.route.parent,
        });
      },
      error: (err: Error) =>
        swal.fire('Ocurrió un error', err.message, 'error'),
    });
  }
}
