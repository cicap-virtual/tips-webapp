import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslocoService } from '@ngneat/transloco';
import { mergeMap, Observable } from 'rxjs';
import { Schedule, Survey } from 'src/app/shared/models/surveys.model';
import { SchedulesService } from 'src/app/shared/services/schedules.service';
import { SurveysService } from 'src/app/shared/services/surveys.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-schedules-new',
  templateUrl: './schedules-new.component.html',
  styleUrls: ['./schedules-new.component.sass'],
})
export class SchedulesNewComponent implements OnInit {
  selectedSurvey$: Observable<Survey>;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private surveyService: SurveysService,
    private scheduleService: SchedulesService,
    private transloco: TranslocoService
  ) {}

  ngOnInit() {
    this.selectedSurvey$ = this.route.params.pipe(
      mergeMap((params) => this.surveyService.getSurvey(params['surveyId']))
    );
  }

  saveSchedule(schedule: Schedule) {
    this.scheduleService.createSchedule(schedule).subscribe({
      next: (res) => {
        swal.fire(
          this.transloco.translate('createdSchedule'),
          this.transloco.translate('success'),
          'success'
        );
        this.router.navigate(['./' + res.id], {
          relativeTo: this.route.parent,
        });
      },
      error: (err) => console.error(err),
    });
  }
}
