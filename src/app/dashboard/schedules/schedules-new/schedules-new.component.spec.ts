import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoModule } from '@ngneat/transloco';

import { SchedulesFormComponent } from '../schedules-form/schedules-form.component';
import { SchedulesNewComponent } from './schedules-new.component';

describe('SchedulesNewComponent', () => {
  let component: SchedulesNewComponent;
  let fixture: ComponentFixture<SchedulesNewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslocoModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
        NgbModalModule,
      ],
      declarations: [SchedulesNewComponent, SchedulesFormComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulesNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
