import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { SchedulesDetailsComponent } from './schedules-details/schedules-details.component';
import { SchedulesEditComponent } from './schedules-edit/schedules-edit.component';
import { SchedulesHomeComponent } from './schedules-home/schedules-home.component';
import { SchedulesNewComponent } from './schedules-new/schedules-new.component';
import { SchedulesComponent } from './schedules.component';

const routes: Routes = [
  {
    path: '',
    component: SchedulesComponent,
    children: [
      { path: '', component: SchedulesHomeComponent },
      { path: 'new', component: SchedulesNewComponent },
      { path: ':id/edit', component: SchedulesEditComponent },
      { path: ':id', component: SchedulesDetailsComponent },
      { path: '**', redirectTo: '', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class SchedulesRoutingModule {}
