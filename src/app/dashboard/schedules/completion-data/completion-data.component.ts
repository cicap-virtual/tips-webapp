import { Component, Input, OnInit } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { ChartConfiguration, ChartData, ChartType } from 'chart.js';

import { Observable } from 'rxjs';
import { SurveyResult } from 'src/app/shared/models/results.models';
import { Completion, Schedule } from 'src/app/shared/models/surveys.model';
import { SchedulesService } from 'src/app/shared/services/schedules.service';
import { UtilService } from 'src/app/shared/services/util.service';

@Component({
  selector: 'app-completion-data',
  templateUrl: './completion-data.component.html',
  styleUrls: ['./completion-data.component.sass'],
})
export class CompletionDataComponent implements OnInit {
  @Input() schedule: Schedule;
  completions$: Observable<Completion[]>;
  results$: Observable<SurveyResult[]>;
  showData = false;

  public radarChartOptions: ChartConfiguration['options'] = {
    responsive: true,
    scales: {
      r: {
        beginAtZero: true,
      },
    },
    plugins: {
      legend: {
        position: 'left',
        align: 'start',
        labels: {
          font: { family: 'Rubik' },
        },
      },
      tooltip: {
        callbacks: {
          label: (tooltipItem) => {
            let label = tooltipItem.label || '';

            if (label) {
              label += ': ';
            }
            label += Math.round(+tooltipItem.formattedValue * 100) / 100;
            return label;
          },
          title: (tooltipItem) => tooltipItem[0].dataset.label,
        },
      },
    },
  };

  chartData: ChartData<'radar'>;
  public radarChartType: ChartType = 'radar';

  public labels: string[];

  isLoading = true;

  constructor(
    private readonly scheduleService: SchedulesService,
    private util: UtilService,
    private transloco: TranslocoService
  ) {}

  ngOnInit() {
    this.results$ = this.scheduleService.getResults(this.schedule.id);
    this.results$.subscribe({
      next: (results) => {
        if (results.length) {
          this.labels = results[0].measures.map((x) => x.name);
          const datasets = [];
          results.forEach((x) => {
            const dataset = {
              data: x.measures.map((y) => y.value),
              label: x.profile?.name,
            };
            datasets.push(dataset);
          });

          const group: number[][] = new Array(results[0].measures.length);

          datasets
            .map((x) => x.data)
            .forEach((x) => {
              x.forEach((y, i) => {
                if (!group[i]) {
                  group[i] = [];
                }
                group[i].push(+x[i]);
              });
            });

          const average = {
            data: group.map((x) => this.util.average(x)),
            label: this.transloco.translate('Average'),
          };
          datasets.push(average);
          datasets.push({
            data: group.map((x) => Math.max(...x)),
            label: this.transloco.translate('Max'),
          });
          datasets.push({
            data: group.map((x) => Math.min(...x)),
            label: this.transloco.translate('Min'),
          });
          this.chartData = { labels: this.labels, datasets };
          this.showData = true;
        }

        this.isLoading = false;
      },
    });
  }
}
