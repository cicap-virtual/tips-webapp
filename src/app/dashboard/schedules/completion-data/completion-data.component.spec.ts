import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CompletionDataComponent } from './completion-data.component';

describe('CompletionDataComponent', () => {
  let component: CompletionDataComponent;
  let fixture: ComponentFixture<CompletionDataComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletionDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletionDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
