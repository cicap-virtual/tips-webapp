import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslocoModule } from '@ngneat/transloco';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { LoadingModalModule } from 'src/app/shared/components/loading-modal/loading-modal.module';

import { SchedulesDetailsComponent } from './schedules-details.component';

describe('SchedulesDetailsComponent', () => {
  let component: SchedulesDetailsComponent;
  let fixture: ComponentFixture<SchedulesDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslocoModule,
        NgxChartsModule,
        LoadingModalModule,
        HttpClientTestingModule
      ],
      declarations: [SchedulesDetailsComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulesDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
