import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslocoService } from '@ngneat/transloco';
import { Observable } from 'rxjs';
import { map, mergeMap, tap } from 'rxjs/operators';
import { Participant, Schedule } from 'src/app/shared/models/surveys.model';
import { SchedulesService } from 'src/app/shared/services/schedules.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-schedules-details',
  templateUrl: './schedules-details.component.html',
  styleUrls: ['./schedules-details.component.sass'],
})
export class SchedulesDetailsComponent implements OnInit {
  schedule$: Observable<Schedule>;
  public participant: Participant;
  chartData: any[] = [];

  colorScheme = {
    domain: ['#5AA454', '#9DAAAE'],
  };
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private scheduleService: SchedulesService,
    private transloco: TranslocoService
  ) {}

  ngOnInit() {
    this.schedule$ = this.route.params.pipe(
      mergeMap((params) =>
        this.scheduleService.getSchedule(params['id']).pipe(
          tap(
            (schedule) =>
              (this.chartData = [
                {
                  name: this.transloco.translate('completed'),
                  value: schedule.completed,
                },
                {
                  name: this.transloco.translate('pending'),
                  value: schedule.participantsCount - schedule.completed,
                },
              ])
          )
        )
      )
    );
  }

  async deleteSchedule(id: string) {
    const result = await swal.fire<Promise<boolean>>({
      title: this.transloco.translate('Want to delete survey?'),
      text: this.transloco.translate('The survey dont will be deleted'),
      icon: 'question',
      showCancelButton: true,
      confirmButtonColor: '#7f5e86',
      cancelButtonColor: '#bfbfbf',
      confirmButtonText: this.transloco.translate('Yes, delete'),
      cancelButtonText: this.transloco.translate('No'),
    });
    if (result.isConfirmed) {
      this.scheduleService.deleteSchedule(id).subscribe({
        next: () => {
          swal.fire(this.transloco.translate('Schedule removed'), '', 'info');
          this.router.navigate(['./'], {
            relativeTo: this.route.parent,
          });
        },
        error: (err) => console.error(err),
      });
    }
  }
}
