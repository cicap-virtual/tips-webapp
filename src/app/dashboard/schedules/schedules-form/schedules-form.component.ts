import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TableOptions } from '@skooltrak/custom-components';
import { Observable } from 'rxjs';
import { UsersAssignComponent } from 'src/app/shared/components/users-assign/users-assign.component';
import { Company } from 'src/app/shared/models/companies.model';
import { Profile, Schedule, Survey } from 'src/app/shared/models/surveys.model';
import { CompaniesService } from 'src/app/shared/services/companies.service';
import { SurveysService } from 'src/app/shared/services/surveys.service';

@Component({
  selector: 'app-schedules-form',
  templateUrl: './schedules-form.component.html',
  styleUrls: ['./schedules-form.component.sass'],
})
export class SchedulesFormComponent implements OnInit {
  @Input() schedule: Schedule = {};
  @Output() saveSchedule = new EventEmitter();

  table = new TableOptions();

  scheduleForm: FormGroup;
  companies$: Observable<Company[]>;
  selectedCompany: Company;

  surveys$: Observable<Survey[]>;

  constructor(
    private modal: NgbModal,
    private fb: FormBuilder,
    private surveyService: SurveysService,
    private companiesServ: CompaniesService
  ) {}

  ngOnInit() {
    this.table.searchable = false;
    this.table.columns = [
      { name: 'name', title: 'Nombre' },
      { name: 'email', title: 'email' },
    ];
    this.companies$ = this.companiesServ.getAll();
    this.scheduleForm = this.fb.group({
      id: [this.schedule ? this.schedule.id : ''],
      survey: [
        this.schedule ? this.schedule.survey : undefined,
        [Validators.required],
      ],
      description: [
        this.schedule ? this.schedule.description : '',
        [Validators.required],
      ],
      startDate: [
        this.schedule ? this.schedule.startDate : '',
        [Validators.required],
      ],
      createDate: [this.schedule ? this.schedule.createDate : ''],
      dueDate: [
        this.schedule ? this.schedule.dueDate : '',
        [Validators.required],
      ],
      participants: [this.schedule ? this.schedule.participants : ''],
    });

    this.surveys$ = this.surveyService.getSurveys();
  }

  assignUser() {
    let target;
    // eslint-disable-next-line prefer-const
    target = Object.assign([], target, this.schedule.participants);
    const modalRef = this.modal.open(UsersAssignComponent, { size: 'lg' });
    modalRef.componentInstance.selectedUsers = target;
    modalRef.componentInstance.newSelection.subscribe((val: Profile[]) => {
      this.schedule.participants = [];
      this.schedule.participants = Object.assign(
        [],
        this.schedule.participants,
        val
      );
    });
  }

  save() {
    this.scheduleForm.controls['participants'].setValue(
      this.schedule.participants
    );
    this.saveSchedule.emit(this.scheduleForm.value);
  }

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
}
