import { DatePipe } from '@angular/common';
import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { ChartConfiguration, ChartData, ChartType } from 'chart.js';
import * as pdfMake from 'pdfmake/build/pdfmake.js';
import { Observable } from 'rxjs';
import { SurveyResult } from 'src/app/shared/models/results.models';
import { Completion } from 'src/app/shared/models/surveys.model';
import { CompletionService } from 'src/app/shared/services/completion.service';
import { UtilService } from 'src/app/shared/services/util.service';

@Component({
  selector: 'app-completion-details',
  templateUrl: './completion-details.component.html',
  styleUrls: ['./completion-details.component.sass'],
})
export class CompletionDetailsComponent implements OnInit, OnChanges {
  @Input() completionId: string;
  isLoading = true;
  result: SurveyResult;
  completion$: Observable<Completion>;

  public radarChartOptions: ChartConfiguration['options'] = {
    responsive: true,
    scales: {
      r: {
        beginAtZero: true,
      },
    },
    plugins: {
      legend: {
        position: 'left',
        align: 'start',
        labels: {
          font: { family: 'Rubik' },
        },
      },
      tooltip: {
        callbacks: {
          label: (tooltipItem) => {
            console.log('tooltipItem: ', tooltipItem);
            let label = tooltipItem.label || '';

            if (label) {
              label += ': ';
            }
            label += Math.round(+tooltipItem.formattedValue * 100) / 100;
            return label;
          },
          title: (tooltipItem) => tooltipItem[0].dataset.label,
        },
      },
    },
  };

  chartData: ChartData<'radar'>;
  public radarChartType: ChartType = 'radar';

  public labels: string[];

  noCompletion: boolean;
  date = new Date();

  constructor(
    private readonly completionServ: CompletionService,
    private transloco: TranslocoService,
    private datePipe: DatePipe,
    private util: UtilService
  ) {}

  ngOnInit() {}

  async generatePDF() {
    const documentDefinition: any = await this.getDocument();
    pdfMake.createPdf(documentDefinition).print();
  }

  async getDocument() {
    const canvas = document.getElementById('chart-canvas') as HTMLCanvasElement;
    const chartData = canvas.toDataURL();
    return {
      info: {
        title: `Resultado ${this.result.survey.name} - ${this.result.profile.name}`,
        author: 'CICAP Consultores',
        subject: `Informe de Resultado ${this.result.survey.name} - ${this.result.profile.name}`,
        keywords: `CICAP Consultores ${this.result.survey.name} ${this.result.profile.name}`,
      },
      watermark: { text: 'CONFIDENCIAL', fontSize: 30, opacity: 0.1 },
      header: {
        columns: [
          {
            columns: [
              {
                image: await this.util.getBase64ImageFromURL(
                  'assets/img/logo-horizontal.png'
                ),
                width: 100,
              },
            ],
            width: 175,
            margin: [20, 20],
          },
          {
            text: this.result.survey.name,
            margin: [20, 20],
            style: 'header',
            alignment: 'center',
          },
          {
            text: `Fecha de impresión: ${this.date.toLocaleString()}`,
            margin: [20, 20],
            width: 175,
            fontSize: 8,
            alignment: 'right',
          },
        ],
      },
      pageMargins: [20, 80, 20, 60],
      content: [
        {
          text: [
            { text: this.transloco.translate('Employee') + ': ', bold: true },
            { text: this.result.profile.name },
          ],
          style: 'info',
        },
        {
          text: [
            {
              text: this.transloco.translate('Company') + ': ',
              bold: true,
            },
            {
              text: this.result.profile.company
                ? this.result.profile.company.name
                : '',
            },
          ],
          style: 'info',
        },
        {
          text: [
            { text: this.transloco.translate('Position') + ': ', bold: true },
            {
              text: this.result.profile.position
                ? this.result.profile.position.name
                : '',
            },
          ],
          style: 'info',
        },
        {
          text: [
            { text: this.transloco.translate('Email') + ': ', bold: true },
            { text: this.result.profile.email },
          ],
          style: 'info',
        },
        {
          text: [
            {
              text: this.transloco.translate('completionDate') + ': ',
              bold: true,
            },
            {
              text: this.datePipe.transform(this.result.completeDate, 'medium'),
            },
          ],
          style: 'info',
        },
        {
          image: chartData,
          width: 500,
          margin: [0, 20],
        },
        {
          layout: 'lightHorizontalLines',
          margin: [60, 20],
          table: {
            headerRows: 1,
            widths: ['*', '*'],
            body: this.getValues(),
          },
        },
      ],
      styles: {
        info: { margin: [0, 2] },
        header: { fontSize: 18, bold: true },
        tableHeader: { bold: true, fillColor: '#287FB9', color: '#fff' },
        anotherStyle: { fontSize: 13, italics: true, alignment: 'right' },
      },
      footer: {
        text: `Fecha de impresión: ${this.date.toLocaleString()}`,
        margin: [20, 20],
        alignment: 'right',
      },
    };
  }

  getValues() {
    const array: string[][] = [];
    array.push([
      this.transloco.translate('measure'),
      this.transloco.translate('Value'),
    ]);
    this.result.measures.forEach((x) => {
      const element = [];
      element.push(x.name);
      element.push(x.value.toString());
      array.push(element);
    });
    return array;
  }

  ngOnChanges(model: SimpleChanges) {
    if (model.completionId) {
      if (this.completionId) {
        this.noCompletion = false;
        this.completion$ = this.completionServ.getCompletion(this.completionId);
        this.completionServ.getResult(this.completionId).subscribe({
          next: (res) => {
            this.result = res;
            this.chartData = {
              labels: res.measures.map((x) => x.name),
              datasets: [
                {
                  data: this.result.measures.map((x) => x.value),
                  label: 'Rueda',
                  backgroundColor: 'rgba(247,70,74,0.2)',
                  borderColor: 'rgba(247,70,74,1)',
                },
                {
                  data: this.result.measures.map((x) => x.subMeasuresValue),
                  label: 'SubRueda',
                  backgroundColor: 'rgba(70,191,189,0.2)',
                  borderColor: 'rgba(70,191,189,1)',
                },
                {
                  data: this.result.measures.map((x) =>
                    Math.abs(x.subMeasuresValue - x.value)
                  ),
                  label: 'Brecha',
                  backgroundColor: 'rgba(253,180,92,0.2)',
                  borderColor: 'rgba(253,180,92,1)',
                },
              ],
            };
            this.labels = res.measures.map((x) => x.name);
            this.isLoading = false;
          },
          error: (err) => console.error(err),
        });
      } else {
        this.noCompletion = true;
      }
    }
  }
}
