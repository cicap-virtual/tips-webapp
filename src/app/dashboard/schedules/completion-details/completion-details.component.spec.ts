import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CompletionDetailsComponent } from './completion-details.component';

describe('CompletionDetailsComponent', () => {
  let component: CompletionDetailsComponent;
  let fixture: ComponentFixture<CompletionDetailsComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletionDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletionDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
