import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoModule } from '@ngneat/transloco';
import { CustomComponentsModule } from '@skooltrak/custom-components';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { CalendarModule } from 'angular-calendar';
import { NgChartsModule } from 'ng2-charts';
import { CalendarHeaderModule } from 'src/app/shared/components/calendar-header/calendar-header.module';
import { LoadingModalModule } from 'src/app/shared/components/loading-modal/loading-modal.module';
import { UsersAssignModule } from 'src/app/shared/components/users-assign/users-assign.module';

import { CompletionDataComponent } from './completion-data/completion-data.component';
import { CompletionDetailsComponent } from './completion-details/completion-details.component';
import { SchedulesCalendarComponent } from './schedules-calendar/schedules-calendar.component';
import { SchedulesDetailsComponent } from './schedules-details/schedules-details.component';
import { SchedulesEditComponent } from './schedules-edit/schedules-edit.component';
import { SchedulesFormComponent } from './schedules-form/schedules-form.component';
import { SchedulesHomeComponent } from './schedules-home/schedules-home.component';
import { SchedulesListComponent } from './schedules-list/schedules-list.component';
import { SchedulesNewComponent } from './schedules-new/schedules-new.component';
import { SchedulesComponent } from './schedules.component';
import { SchedulesRoutingModule } from './schedules.routes';

@NgModule({
  declarations: [
    SchedulesComponent,
    SchedulesCalendarComponent,
    SchedulesDetailsComponent,
    SchedulesFormComponent,
    SchedulesNewComponent,
    SchedulesEditComponent,
    SchedulesHomeComponent,
    SchedulesListComponent,
    CompletionDetailsComponent,
    CompletionDataComponent,
  ],
  imports: [
    CommonModule,
    CustomComponentsModule,
    SchedulesRoutingModule,
    NgbNavModule,
    NgChartsModule,
    TranslocoModule,
    CalendarHeaderModule,
    CalendarModule,
    LoadingModalModule,
    FormsModule,
    ReactiveFormsModule,
    UsersAssignModule,
    NgxChartsModule,
  ],
  providers: [DatePipe],
})
export class SchedulesModule {}
