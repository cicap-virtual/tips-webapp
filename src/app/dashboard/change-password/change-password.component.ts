import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from 'src/app/shared/services/users.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.sass'],
})
export class ChangePasswordComponent implements OnInit {
  passwordForm: FormGroup;
  constructor(
    private usersServ: UsersService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.passwordForm = this.fb.group(
      {
        password: ['', [Validators.required, Validators.minLength(6)]],
        passwordConfirm: ['', [Validators.required, Validators.minLength(6)]],
      },
      { validators: [this.passwordConfirm] }
    );
  }

  changePassword() {
    this.usersServ
      .changePassword(this.passwordForm.get('password').value)
      .subscribe({
        next: () => {
          swal.fire('Contraseña actualizada exitosamente', '', 'success');
          this.router.navigate(['./'], { relativeTo: this.route.parent });
        },
        error: (err: Error) => {
          swal.fire('Ocurrión un error', err.message, 'error');
        },
      });
  }

  passwordConfirm = (g: FormGroup) =>
    g.get('password').value === g.get('passwordConfirm').value
      ? null
      : { mismatch: true };
}
