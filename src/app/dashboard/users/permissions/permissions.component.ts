import { Component, OnInit } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { TableOptions } from '@skooltrak/custom-components';
import { Observable } from 'rxjs';
import { Access } from 'src/app/shared/models/auth.models';
import { PermissionsService } from 'src/app/shared/services/permissions.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-permissions',
  templateUrl: './permissions.component.html',
  styleUrls: ['./permissions.component.sass'],
})
export class PermissionsComponent implements OnInit {
  permissions$: Observable<Access[]>;
  options = new TableOptions();
  constructor(
    private readonly permissionsServ: PermissionsService,
    private readonly transloco: TranslocoService
  ) {}

  ngOnInit() {
    this.options.creatable = true;
    this.options.searchable = true;
    this.options.columns = [
      { name: 'name', title: 'Nombre', required: true },
      { name: 'code', title: 'Código', required: true },
    ];
    this.permissions$ = this.permissionsServ.getPermissions();
  }

  createPermission(permission: Access) {
    this.permissionsServ.createPermission(permission).subscribe({
      next: (res) => {
        swal.fire(
          this.transloco.translate('Access created'),
          res.name,
          'success'
        );
        this.permissions$ = this.permissionsServ.getPermissions();
      },
    });
  }

  editPermission(permission: Access) {
    this.permissionsServ.editPermission(permission.id, permission).subscribe({
      next: () => {
        swal.fire(
          this.transloco.translate('Access updated'),
          permission.name,
          'success'
        );
        this.permissions$ = this.permissionsServ.getPermissions();
      },
    });
  }

  deletePermission(id: string) {
    this.permissionsServ.deletePermission(id).subscribe({
      next: () => {
        swal.fire(this.transloco.translate('Access removed'), '', 'info');
        this.permissions$ = this.permissionsServ.getPermissions();
      },
    });
  }
}
