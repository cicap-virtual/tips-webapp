import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoModule } from '@ngneat/transloco';
import { CustomComponentsModule } from '@skooltrak/custom-components';
import { LoadingModalModule } from 'src/app/shared/components/loading-modal/loading-modal.module';

import { EditUserComponent } from './edit-user/edit-user.component';
import { NewUserComponent } from './new-user/new-user.component';
import { PermissionsFormComponent } from './permissions-form/permissions-form.component';
import { PermissionsComponent } from './permissions/permissions.component';
import { RolesEditComponent } from './roles-edit/roles-edit.component';
import { RolesFormComponent } from './roles-form/roles-form.component';
import { RolesNewComponent } from './roles-new/roles-new.component';
import { RolesComponent } from './roles/roles.component';
import { UsersFormComponent } from './users-form/users-form.component';
import { UsersHomeComponent } from './users-home/users-home.component';
import { UsersListComponent } from './users-list/users-list.component';
import { UsersComponent } from './users.component';
import { UsersRoutingModule } from './users.routes';
import { RolePermissionsComponent } from './role-permissions/role-permissions.component';

@NgModule({
  declarations: [
    UsersComponent,
    UsersListComponent,
    RolesComponent,
    RolesFormComponent,
    PermissionsComponent,
    UsersHomeComponent,
    RolesNewComponent,
    RolesEditComponent,
    PermissionsFormComponent,
    UsersFormComponent,
    NewUserComponent,
    EditUserComponent,
    RolePermissionsComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    TranslocoModule,
    NgbNavModule,
    CustomComponentsModule,
    LoadingModalModule,
    ReactiveFormsModule
  ],
  providers: [DatePipe]
})
export class UsersModule {}
