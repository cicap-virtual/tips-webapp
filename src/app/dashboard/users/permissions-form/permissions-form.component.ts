import { Component, OnInit, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Access } from 'src/app/shared/models/auth.models';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-permissions-form',
  templateUrl: './permissions-form.component.html',
  styleUrls: ['./permissions-form.component.sass'],
})
export class PermissionsFormComponent implements OnInit {
  @Input() group: FormGroup;
  @Input() permissions$: Observable<Access[]>;

  constructor() {}

  ngOnInit() {}

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
}
