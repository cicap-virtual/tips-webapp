import { Component, OnInit } from '@angular/core';
import { TableOptions } from '@skooltrak/custom-components';
import { Observable } from 'rxjs';
import { Role } from 'src/app/shared/models/auth.models';
import { RolesService } from 'src/app/shared/services/roles.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.sass'],
})
export class RolesComponent implements OnInit {
  roles$: Observable<Role[]>;
  table = new TableOptions();
  constructor(private rolesServ: RolesService) {}

  ngOnInit() {
    this.table.searchable = true;
    this.table.detailsURL = ['roles'];
    this.table.newURL = ['roles', 'new'];

    this.table.columns = [
      { name: 'name', title: 'Nombre', required: true },
      { name: 'isUser', title: 'Usuario', type: 'boolean', required: true },
      {
        name: 'isAdmin',
        title: 'Administrador',
        type: 'boolean',
        required: true,
      },
      {
        name: 'isCompanyAdmin',
        title: 'Adm. Empresa',
        type: 'boolean',
        required: true,
      },
      {
        name: 'isCreator',
        title: 'Elaborador',
        type: 'boolean',
        required: true,
      },
      { name: 'isExternal', title: 'Externo', type: 'boolean', required: true },
    ];
    this.getRoles();
  }

  getRoles() {
    this.roles$ = this.rolesServ.getRoles();
  }

  deleteRole(id: string) {
    this.rolesServ.deleteRole(id).subscribe({
      next: (res) => {
        swal.fire('Rol eliminado', '', 'info');
        this.getRoles();
      },
    });
  }
}
