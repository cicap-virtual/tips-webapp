import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';
import { Access, Permission, Role } from 'src/app/shared/models/auth.models';
import { PermissionsService } from 'src/app/shared/services/permissions.service';

@Component({
  selector: 'app-roles-form',
  templateUrl: './roles-form.component.html',
  styleUrls: ['./roles-form.component.sass'],
})
export class RolesFormComponent implements OnInit {
  @Input() role: Role;
  @Output() save = new EventEmitter();

  permissions$: Observable<Access[]>;
  public roleForm: FormGroup;
  constructor(
    private readonly fb: FormBuilder,
    private permissionServ: PermissionsService
  ) {}

  ngOnInit() {
    this.roleForm = this.fb.group({
      id: [this.role ? this.role.id : ''],
      name: [this.role ? this.role.name : '', [Validators.required]],
      isUser: [this.role ? this.role.isUser : false],
      isCreator: [this.role ? this.role.isCreator : false],
      isAdmin: [this.role ? this.role.isAdmin : false],
      isCompanyAdmin: [this.role ? this.role.isCompanyAdmin : false],
      isExternal: [this.role ? this.role.isExternal : false],
      permissions: this.role
        ? this.fb.array(this.initExisting())
        : this.fb.array([this.initPermission()]),
    });

    this.permissions$ = this.permissionServ.getPermissions();
  }

  initPermission(permission?: Permission): FormGroup {
    return this.fb.group({
      access: [permission ? permission.access : '', [Validators.required]],
      read: [permission ? permission.read : true],
      edit: [permission ? permission.create : true],
      create: [permission ? permission.create : true],
      delete: [permission ? permission.delete : true],
    });
  }

  initExisting(): FormGroup[] {
    const controls: FormGroup[] = [];
    this.role.permissions.forEach((item) => {
      controls.push(this.initPermission(item));
    });
    return controls;
  }

  addPermission(): void {
    const control = this.roleForm.controls['permissions'] as FormArray;
    control.push(this.initPermission());
  }

  removePermission(i: number): void {
    const control = this.roleForm.controls['permissions'] as FormArray;
    control.removeAt(i);
  }

  saveRole() {
    this.save.emit(this.roleForm.value);
  }
}
