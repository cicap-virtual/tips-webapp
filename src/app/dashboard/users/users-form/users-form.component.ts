import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RegexEnum } from '@skooltrak/custom-components';
import { Observable } from 'rxjs';
import { Role, User } from 'src/app/shared/models/auth.models';
import { Company } from 'src/app/shared/models/companies.model';
import { CompaniesService } from 'src/app/shared/services/companies.service';
import { RolesService } from 'src/app/shared/services/roles.service';

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.sass'],
})
export class UsersFormComponent implements OnInit {
  @Input() user: User;
  @Output() save = new EventEmitter<User>();

  userForm: FormGroup;
  roles$: Observable<Role[]>;
  companies$: Observable<Company[]>;

  constructor(
    private fb: FormBuilder,
    private rolesService: RolesService,
    private companiesService: CompaniesService
  ) {}

  ngOnInit() {
    this.userForm = this.fb.group({
      id: [this.user ? this.user.id : ''],
      userName: [this.user ? this.user.userName : '', [Validators.required]],
      email: [
        this.user ? this.user.email : '',
        [Validators.required, Validators.pattern(RegexEnum.EMAIL)],
      ],
      companies: [this.user ? this.user.companies : ''],
      roles: [this.user ? this.user.roles : ''],
      approved: [this.user ? this.user.approved : ''],
    });
    this.roles$ = this.rolesService.getRoles();
    this.companies$ = this.companiesService.getAll();
  }

  isCompanyAdmin(): boolean {
    const roles: Role[] = this.userForm.get('roles').value;
    return roles.filter((x) => x.isCompanyAdmin).length > 0;
  }

  saveForm() {
    this.userForm.markAllAsTouched();
    if (this.userForm.valid) {
      this.save.emit(this.userForm.value);
    }
  }
}
