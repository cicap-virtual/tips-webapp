import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslocoService } from '@ngneat/transloco';
import { mergeMap } from 'rxjs';
import { Role } from 'src/app/shared/models/auth.models';
import { RolesService } from 'src/app/shared/services/roles.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-roles-edit',
  templateUrl: './roles-edit.component.html',
  styleUrls: ['./roles-edit.component.sass'],
})
export class RolesEditComponent implements OnInit {
  role: Role;

  constructor(
    private route: ActivatedRoute,
    private roleServ: RolesService,
    private router: Router,
    private transloco: TranslocoService
  ) {}

  ngOnInit() {
    this.route.params
      .pipe(mergeMap((params) => this.roleServ.getRole(params['id'])))
      .subscribe({
        next: (res) => {
          this.role = res;
        },
        error: (err) => console.error(err),
      });
  }

  save(role: Role) {
    this.roleServ.editRole(role.id, role).subscribe({
      next: (res) => {
        swal.fire(
          this.transloco.translate('Role updated'),
          role.name,
          'success'
        );
        this.router.navigate(['./users'], {
          relativeTo: this.route.parent.parent.parent,
        });
      },
      error: (err) =>
        swal.fire(
          this.transloco.translate('Something went wrong', err.message, 'error')
        ),
    });
  }
}
