import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EditUserComponent } from './edit-user/edit-user.component';
import { NewUserComponent } from './new-user/new-user.component';
import { RolesEditComponent } from './roles-edit/roles-edit.component';
import { RolesNewComponent } from './roles-new/roles-new.component';
import { UsersHomeComponent } from './users-home/users-home.component';
import { UsersComponent } from './users.component';

const routes: Routes = [
  {
    path: '',
    component: UsersComponent,
    children: [
      { path: '', component: UsersHomeComponent },
      { path: ':id', component: EditUserComponent },
      { path: 'new', component: NewUserComponent },
      { path: 'roles/new', component: RolesNewComponent },
      { path: 'roles/:id', component: RolesEditComponent },
      { path: '**', redirectTo: '', pathMatch: 'full' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class UsersRoutingModule {}
