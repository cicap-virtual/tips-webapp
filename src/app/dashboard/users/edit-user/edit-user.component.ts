import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslocoService } from '@ngneat/transloco';
import { mergeMap } from 'rxjs';
import { User } from 'src/app/shared/models/auth.models';
import { UsersService } from 'src/app/shared/services/users.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.sass'],
})
export class EditUserComponent implements OnInit {
  user: User;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UsersService,
    private transloco: TranslocoService
  ) {}

  ngOnInit() {
    this.route.params
      .pipe(mergeMap((params) => this.userService.getUser(params['id'])))
      .subscribe({
        next: (user) => {
          this.user = user;
        },
      });
  }

  saveChanges(user: User) {
    console.log(user);
    this.userService.editUser(user.id, user).subscribe({
      next: () => {
        swal.fire(
          user.userName,
          this.transloco.translate('User updated'),
          'success'
        );
        this.router.navigate(['./'], { relativeTo: this.route.parent });
      },
      error: (err: Error) => {
        swal.fire(
          this.transloco.translate('Something went wrong'),
          this.transloco.translate(err.message),
          'error'
        );
      },
    });
  }
}
