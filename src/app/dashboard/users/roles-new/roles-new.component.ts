import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslocoService } from '@ngneat/transloco';
import { Role } from 'src/app/shared/models/auth.models';
import { RolesService } from 'src/app/shared/services/roles.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-roles-new',
  templateUrl: './roles-new.component.html',
  styleUrls: ['./roles-new.component.sass'],
})
export class RolesNewComponent implements OnInit {
  constructor(
    private rolesServ: RolesService,
    private router: Router,
    private route: ActivatedRoute,
    private transloco: TranslocoService
  ) {}

  ngOnInit() {}

  save(role: Role) {
    this.rolesServ.createRole(role).subscribe({
      next: (res) => {
        swal.fire(
          this.transloco.translate('Role created'),
          res.name,
          'success'
        );
        this.router.navigate(['./users'], {
          relativeTo: this.route.parent.parent.parent,
        });
      },
      error: (err) =>
        swal.fire(
          this.transloco.translate('Something went wrong'),
          err.message,
          'error'
        ),
    });
  }
}
