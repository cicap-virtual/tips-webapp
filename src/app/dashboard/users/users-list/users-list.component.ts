import { Component, OnInit } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { TableOptions } from '@skooltrak/custom-components';
import { Observable } from 'rxjs';
import { Role, User } from 'src/app/shared/models/auth.models';
import { RolesService } from 'src/app/shared/services/roles.service';
import { UsersService } from 'src/app/shared/services/users.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.sass'],
})
export class UsersListComponent implements OnInit {
  users$: Observable<User[]>;
  table = new TableOptions();
  roles$: Observable<Role[]>;

  constructor(
    private usersServ: UsersService,
    private roleServ: RolesService,
    private transloco: TranslocoService
  ) {}

  ngOnInit() {
    this.table.searchable = true;
    this.roles$ = this.roleServ.getRoles();

    this.table.accessCode = 'users';
    this.table.columns = [
      { name: 'userName', title: 'Nombre', filterable: true, required: true },
      {
        name: 'roles',
        title: 'Roles',
        hidden: true,
        objectText: 'name',
        type: 'array',
        asyncList: this.roles$,
      },
      { name: 'email', title: 'Email', required: true, type: 'email' },
      {
        name: 'logged',
        title: this.transloco.translate('Logged'),
        type: 'boolean',
        readonly: true,
      },
      {
        name: 'approved',
        title: this.transloco.translate('Approved'),
        type: 'boolean',
      },
      {
        name: 'createdDate',
        title: this.transloco.translate('Created date'),
        type: 'datetime',
        readonly: true,
      },
      {
        name: 'modificateDate',
        title: this.transloco.translate('Modificated date'),
        type: 'datetime',
        readonly: true,
      },
    ];
    this.table.detailsURL = [];
    this.getUsers();
  }

  createUser(user: User) {
    this.usersServ.createUser(user).subscribe({
      next: (res) => {
        swal.fire('Usuario creado', res.userName, 'success');
        this.getUsers();
      },
      error: (err) =>
        swal.fire(
          this.transloco.translate('Something went wrong', err.message, 'error')
        ),
    });
  }

  editUser(user: User) {
    this.usersServ.editUser(user.id, user).subscribe({
      next: (res) => {
        swal.fire('Usuario actualizado', user.userName, 'success');
        this.getUsers();
      },
      error: (err) =>
        swal.fire(
          this.transloco.translate('Something went wrong', err.message, 'error')
        ),
    });
  }

  deleteUser(id: string) {
    this.usersServ.deleteUser(id).subscribe({
      next: (res) => {
        swal.fire('Usuario eliminado', '', 'info');
        this.getUsers();
      },
      error: (err) =>
        swal.fire(
          this.transloco.translate('Something went wrong', err.message, 'error')
        ),
    });
  }

  getUsers(): void {
    this.users$ = this.usersServ.getUsers();
  }
}
