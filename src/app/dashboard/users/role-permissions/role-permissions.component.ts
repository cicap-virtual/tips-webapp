/* eslint-disable @angular-eslint/component-selector */
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { Access } from 'src/app/shared/models/auth.models';

@Component({
  selector: '[rolePermissions]',
  templateUrl: './role-permissions.component.html',
  styleUrls: ['./role-permissions.component.sass'],
})
export class RolePermissionsComponent implements OnInit {
  @Input() group: FormGroup;
  @Input() permissions$: Observable<Access[]>;

  @Output() delete = new EventEmitter();
  constructor() {}

  ngOnInit(): void {}

  compareFn(c1: any, c2: any): boolean {
    return c1 && c2 ? c1.id === c2.id : c1 === c2;
  }
}
