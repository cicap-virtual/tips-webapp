import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ProfilesService } from 'src/app/shared/services/profiles.service';
import { SchedulesService } from 'src/app/shared/services/schedules.service';
import { SurveysService } from 'src/app/shared/services/surveys.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass'],
})
export class HomeComponent implements OnInit {
  profiles$: Observable<number>;
  surveys$: Observable<number>;
  schedules$: Observable<number>;
  constructor(
    private profileServ: ProfilesService,
    private scheduleServ: SchedulesService,
    private surveyServ: SurveysService
  ) {}

  ngOnInit() {
    this.profiles$ = this.profileServ.getCount();
    this.surveys$ = this.surveyServ.getCount();
    this.schedules$ = this.scheduleServ.getCount();
  }
}
