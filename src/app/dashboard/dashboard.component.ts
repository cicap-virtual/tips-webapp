import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { Role } from '../shared/models/auth.models';
import { selectRole } from '../store/actions';
import { selectAuthInfo, selectOtherRoles } from '../store/selectors';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass'],
})
export class DashboardComponent implements OnInit {
  public active = false;
  public roles$ = this.store$.select(selectOtherRoles);
  public auth$ = this.store$.select(selectAuthInfo);
  constructor(private store$: Store) {}

  ngOnInit(): void {}

  changeRole = (role: Role) => this.store$.dispatch(selectRole({ role }));
}
