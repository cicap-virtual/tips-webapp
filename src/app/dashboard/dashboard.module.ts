import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslocoModule } from '@ngneat/transloco';

import { BreadcrumbModule } from '../shared/components/breadcrumb/breadcrumb.module';
import { FooterModule } from '../shared/components/footer/footer.module';
import { HomeCalendarModule } from '../shared/components/home-calendar/home-calendar.module';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard.routes';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [DashboardComponent, HomeComponent, ChangePasswordComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FooterModule,
    TranslocoModule,
    BreadcrumbModule,
    HomeCalendarModule,
    ReactiveFormsModule,
  ],
})
export class DashboardModule {}
