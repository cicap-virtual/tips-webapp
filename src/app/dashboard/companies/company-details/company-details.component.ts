import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { Company } from 'src/app/shared/models/companies.model';
import { CompaniesService } from 'src/app/shared/services/companies.service';

@Component({
  selector: 'app-company-details',
  templateUrl: './company-details.component.html',
  styleUrls: ['./company-details.component.sass'],
})
export class CompanyDetailsComponent implements OnInit {
  company$: Observable<Company>;
  companyId: string;

  constructor(
    private route: ActivatedRoute,
    private companiesServ: CompaniesService
  ) {}

  ngOnInit() {
    this.route.params.subscribe({
      next: (params) => {
        this.company$ = this.companiesServ.get(params['id']);
        this.companyId = params['id'];
      },
      error: (err) => console.error(err),
    });
  }
}
