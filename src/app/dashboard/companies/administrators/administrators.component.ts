import { Component, Input, OnInit } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { Observable } from 'rxjs';
import { TableOptions } from '@skooltrak/custom-components';
import { User } from 'src/app/shared/models/auth.models';
import { CompaniesService } from 'src/app/shared/services/companies.service';

@Component({
  selector: 'app-administrators',
  templateUrl: './administrators.component.html',
  styleUrls: ['./administrators.component.sass']
})
export class AdministratorsComponent implements OnInit {
  @Input() companyId: string;

  table = new TableOptions();
  administrators$: Observable<User[]>;
  constructor(
    private companiesService: CompaniesService,
    private transloco: TranslocoService
  ) {}

  ngOnInit() {
    this.table.columns = [
      { name: 'userName', title: 'Nombre', filterable: true},
      { name: 'email', title: 'Email', filterable: true },
      {
        name: 'logged',
        title: this.transloco.translate('Logged'),
        type: 'boolean',
        readonly: true
      },
      {
        name: 'approved',
        title: this.transloco.translate('Approved'),
        type: 'boolean'
      },
      {
        name: 'createdDate',
        title: this.transloco.translate('Created date'),
        type: 'datetime',
        readonly: true
      },
      {
        name: 'modificateDate',
        title: this.transloco.translate('Modificated date'),
        type: 'datetime',
        readonly: true
      }
    ];

    this.administrators$ = this.companiesService.getAdministrators(this.companyId);
  }
}
