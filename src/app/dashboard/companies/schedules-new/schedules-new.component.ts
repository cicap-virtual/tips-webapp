import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SchedulesService } from 'src/app/shared/services/schedules.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-schedules-new',
  templateUrl: './schedules-new.component.html',
  styleUrls: ['./schedules-new.component.sass'],
})
export class SchedulesNewComponent implements OnInit {
  @Input() companyId: string;

  schedulesForm: FormGroup;
  constructor(
    public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private schedulesServ: SchedulesService
  ) {}

  ngOnInit() {
    this.schedulesForm = this.fb.group({
      id: [''],
      company: [],
      survey: ['', [Validators.required]],
      description: ['', [Validators.required]],
      startDate: ['', [Validators.required]],
      dueDate: ['', [Validators.required]],
      participants: [[]],
      hierarchies: [[]],
    });
  }

  saveSchedule() {
    this.schedulesForm.controls['company'].setValue({ id: this.companyId });
    this.schedulesServ.createSchedule(this.schedulesForm.value).subscribe({
      next: (res) => {
        swal.fire('Encuesta programada exitosamente', '', 'success');
        this.activeModal.close();
      },
      error: (err) => console.error(err),
    });
  }
}
