import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SchedulesNewComponent } from './schedules-new.component';

describe('SchedulesNewComponent', () => {
  let component: SchedulesNewComponent;
  let fixture: ComponentFixture<SchedulesNewComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedulesNewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulesNewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
