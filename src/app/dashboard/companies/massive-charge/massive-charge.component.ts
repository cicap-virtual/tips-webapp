import { Component, Input } from '@angular/core';
import { Company } from 'src/app/shared/models/companies.model';
import { CompaniesService } from 'src/app/shared/services/companies.service';
import Swal from 'sweetalert2';
import * as XLSX from 'xlsx';

type AOA = any[][];
type ProfileItem = { name: string; email: string; code?: string };

@Component({
  selector: 'app-massive-charge',
  templateUrl: './massive-charge.component.html',
  styleUrls: ['./massive-charge.component.sass'],
})
export class MassiveChargeComponent {
  @Input() company: Company;

  public data: AOA;
  constructor(private companiesService: CompaniesService) {}

  onFileChange(event: any): void {
    const target: DataTransfer = event.target as DataTransfer;

    if (target.files.length !== 1) {
      throw new Error('Cannot use multiple files');
    }
    const reader: FileReader = new FileReader();

    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      this.data = XLSX.utils.sheet_to_json(ws, { header: 1 }) as AOA;
    };
    reader.readAsBinaryString(target.files[0]);
  }

  upload() {
    const items: ProfileItem[] = this.data.map((x) => ({
      name: x[0],
      email: x[1],
      code: x[2],
    }));

    const count = items.length;
    let completed = 0;
    let current = 1;
    Swal.fire({
      title: 'Cargando colaboradores',
      html: `Cargando ${current} de ${count}`,
    }).then((result) => {
      if (result.dismiss === Swal.DismissReason.timer) {
        console.log('I was closed by the timer');
      }
    });

    items.forEach(async (item) => {
      this.companiesService.createProfile(this.company.id, item).subscribe({
        next: () => {
          completed++;
          current++;
        },
        error: () => current++,
        complete: () => {
          if (current === count) {
            Swal.fire(
              'Carga terminada',
              `Cargados: ${completed} de ${count}`,
              'success'
            );
          }
        },
      });
    });
  }
}
