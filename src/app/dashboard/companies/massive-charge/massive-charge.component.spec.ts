import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MassiveChargeComponent } from './massive-charge.component';

describe('MassiveChargeComponent', () => {
  let component: MassiveChargeComponent;
  let fixture: ComponentFixture<MassiveChargeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MassiveChargeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MassiveChargeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
