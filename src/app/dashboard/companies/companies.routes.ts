import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CompaniesHomeComponent } from './companies-home/companies-home.component';
import { CompaniesComponent } from './companies.component';
import { CompanyDetailsComponent } from './company-details/company-details.component';

const routes: Routes = [
  {
    path: '',
    component: CompaniesComponent,
    children: [
      { path: '', component: CompaniesHomeComponent },
      { path: ':id', component: CompanyDetailsComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class CompaniesRoutingModule {}
