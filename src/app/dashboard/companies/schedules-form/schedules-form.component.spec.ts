import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { SchedulesFormComponent } from './schedules-form.component';

describe('SchedulesFormComponent', () => {
  let component: SchedulesFormComponent;
  let fixture: ComponentFixture<SchedulesFormComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ SchedulesFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SchedulesFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
