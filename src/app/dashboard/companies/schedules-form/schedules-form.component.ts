import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { TableOptions } from '@skooltrak/custom-components';
import { Organization } from 'src/app/shared/models/companies.model';
import {
  Profile,
  Survey,
  Participant,
} from 'src/app/shared/models/surveys.model';
import { CompaniesService } from 'src/app/shared/services/companies.service';
import { SurveysService } from 'src/app/shared/services/surveys.service';

@Component({
  selector: 'app-schedules-form',
  templateUrl: './schedules-form.component.html',
  styleUrls: ['./schedules-form.component.sass'],
})
export class SchedulesFormComponent implements OnInit {
  @Input() companyId: string;
  @Input() scheduleForm: FormGroup;

  organization$: Observable<Organization[]>;
  table = new TableOptions();
  employees$: Observable<Profile[]>;
  surveys$: Observable<Survey[]>;
  constructor(
    public surveysServ: SurveysService,
    private companyServ: CompaniesService
  ) {}

  ngOnInit() {
    this.initTable();
    this.organization$ = this.companyServ.getOrganization(this.companyId);
    this.employees$ = this.companyServ.getProfiles(this.companyId);
  }

  initTable() {
    this.table.type = 'select';
    this.surveys$ = this.surveysServ.getSurveys();
    this.table.columns = [
      { name: 'name', title: 'Nombre', filterable: true },
      {
        name: 'parent',
        title: 'Responde a',
        lookup: true,
        type: 'object',
        listDisplay: 'name',
        objectText: 'position.parent.name',
        objectColumn: 'position.parent.name',
        readonly: true,
      },
      {
        name: 'position',
        title: 'Cargo',
        lookup: true,
        type: 'object',
        listDisplay: 'name',
        objectText: 'position.name',
        objectColumn: 'position.name',
      },
    ];
  }

  changeSelection(ids: string[]) {
    this.scheduleForm.get('hierarchies').setValue(ids);
    this.scheduleForm.get('participants').setValue([]);
  }

  changeTableSelection(participants: Participant[]) {
    this.scheduleForm.get('participants').setValue(participants);

    this.scheduleForm.get('hierarchies').setValue([]);
  }
}
