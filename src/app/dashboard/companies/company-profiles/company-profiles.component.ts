import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { TableOptions } from '@skooltrak/custom-components';
import { Observable } from 'rxjs';
import { Company, Hierarchy } from 'src/app/shared/models/companies.model';
import { Profile } from 'src/app/shared/models/surveys.model';
import { CompaniesService } from 'src/app/shared/services/companies.service';
import { ProfilesService } from 'src/app/shared/services/profiles.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-company-profiles',
  templateUrl: './company-profiles.component.html',
  styleUrls: ['./company-profiles.component.sass'],
})
export class CompanyProfilesComponent implements OnInit {
  @Input() company: Company;

  table = new TableOptions();
  positions$: Observable<Hierarchy[]>;

  profiles$: Observable<Profile[]>;
  constructor(
    private companiesService: CompaniesService,
    private profilesService: ProfilesService,
    private transloco: TranslocoService
  ) {}

  ngOnInit() {
    this.positions$ = this.companiesService.getPositions(this.company.id);
    this.table.lookup = true;
    this.table.creatable = true;
    this.table.columns = [
      {
        name: 'name',
        title: this.transloco.translate('Name'),
        filterable: true,
        required: true,
      },
      {
        name: 'email',
        title: 'Email',
        type: 'email',
        required: true,
        filterable: true,
      },
      {
        name: 'parent',
        title: this.transloco.translate('Parent position'),
        lookup: true,
        type: 'object',
        listDisplay: 'name',
        objectText: 'position.parent.name',
        objectColumn: 'position.parent.name',
        readonly: true,
      },
      {
        name: 'position',
        title: this.transloco.translate('Position'),
        lookup: true,
        type: 'object',
        listDisplay: 'name',
        asyncList: this.positions$,
        objectText: 'position.name',
        objectColumn: 'position.name',
      },
      {
        name: 'mobilePhone',
        title: this.transloco.translate('Mobile phone'),
        filterable: true,
        type: 'mobile-phone',
      },
      {
        name: 'officePhone',
        title: this.transloco.translate('Office phone'),
        filterable: true,
        type: 'home-phone',
      },
      {
        name: 'homePhone',
        title: this.transloco.translate('Home phone'),
        filterable: true,
        type: 'home-phone',
        hidden: true,
      },
    ];
    this.profiles$ = this.companiesService.getProfiles(this.company.id);
  }

  createProfile(profile: Profile) {
    profile.company = { id: this.company.id, name: this.company.name };
    this.profilesService.createProfile(profile).subscribe({
      next: (res) => {
        Swal.fire(res.name, 'Colaborador creado exitosamente', 'success');
        this.profiles$ = this.companiesService.getProfiles(this.company.id);
      },
      error: (err: HttpErrorResponse) => {
        if (err.status === 401) {
          Swal.fire(
            this.transloco.translate('The user could not be created'),
            this.transloco.translate(
              'An user with the exact email already exists'
            ),
            'error'
          );
        }
      },
    });
  }

  editProfile(profile: Profile) {
    this.profilesService.editProfile(profile.id, profile).subscribe({
      next: (res) => {
        Swal.fire(profile.name, 'Colaborador creado exitosamente', 'success');
        this.profiles$ = this.companiesService.getProfiles(this.company.id);
      },
      error: (err: HttpErrorResponse) => {
        Swal.fire(
          this.transloco.translate('Something went wrong'),
          this.transloco.translate(err.message),
          'error'
        );
      },
    });
  }
}
