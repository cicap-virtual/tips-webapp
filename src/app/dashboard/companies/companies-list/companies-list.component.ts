import { Component, OnInit } from '@angular/core';
import { TableOptions } from '@skooltrak/custom-components';
import { Observable } from 'rxjs';
import { Company, Industry } from 'src/app/shared/models/companies.model';
import { CompaniesService } from 'src/app/shared/services/companies.service';
import { IndustriesService } from 'src/app/shared/services/industries.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-companies-list',
  templateUrl: './companies-list.component.html',
  styleUrls: ['./companies-list.component.sass'],
})
export class CompaniesListComponent implements OnInit {
  companies$: Observable<Company[]>;
  industries$: Observable<Industry[]>;

  table = new TableOptions();
  constructor(
    private readonly companyServ: CompaniesService,
    private industryServ: IndustriesService
  ) {}

  ngOnInit() {
    this.industries$ = this.industryServ.getAll();
    this.table.searchable = true;
    this.table.detailsURL = [];
    this.table.columns = [
      { name: 'name', title: 'Nombre', filterable: true, required: true },
      {
        name: 'fullName',
        title: 'Nombre completo',
        filterable: true,
        required: true,
      },
      {
        name: 'industries',
        title: 'Industrias',
        asyncList: this.industries$,
        type: 'array',
        objectText: 'name',
      },
      {
        name: 'establishedDate',
        title: 'Fecha de est.',
        type: 'date',
      },
    ];
    this.companies$ = this.companyServ.getAll();
  }

  createCompany(department: Company) {
    this.companyServ.create(department).subscribe({
      next: (res) => {
        swal.fire('Empresa creada exitosamente', res.name, 'success');
        this.companies$ = this.companyServ.getAll();
      },
      error: (err) => swal.fire('Empresa no fue creada', err.message, 'error'),
    });
  }

  editCompany(department: Company) {
    this.companyServ.edit(department.id, department).subscribe({
      next: () => {
        swal.fire('Empresa editada exitosamente', '', 'success');
        this.companies$ = this.companyServ.getAll();
      },
      error: (err) => swal.fire('Empresa no fue creada', err.message, 'error'),
    });
  }

  deleteCompany(id: string) {
    this.companyServ.delete(id).subscribe({
      next: () => {
        swal.fire('Empresa eliminada exitosamente', '', 'info');
        this.companies$ = this.companyServ.getAll();
      },
      error: (err) => swal.fire('Empresa no fue creada', err.message, 'error'),
    });
  }
}
