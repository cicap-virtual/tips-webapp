import { Component, OnInit } from '@angular/core';
import { TableOptions } from '@skooltrak/custom-components';
import { Observable } from 'rxjs';
import { Industry } from 'src/app/shared/models/companies.model';
import { IndustriesService } from 'src/app/shared/services/industries.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-industries',
  templateUrl: './industries.component.html',
  styleUrls: ['./industries.component.sass'],
})
export class IndustriesComponent implements OnInit {
  industries$: Observable<Industry[]>;
  table = new TableOptions();
  constructor(private industryServ: IndustriesService) {}

  ngOnInit() {
    this.table.creatable = true;
    this.table.searchable = true;
    this.table.columns = [
      {
        name: 'name',
        title: 'Nombre',
        sortable: true,
        filterable: true,
        required: true,
      },
    ];

    this.industries$ = this.industryServ.getAll();
  }

  create(industry: Industry) {
    this.industryServ.create(industry).subscribe({
      next: (res) => {
        this.industries$ = this.industryServ.getAll();
        swal.fire('Industria creada', res.name, 'success');
      },
      error: (err) => console.error(err),
    });
  }

  edit(industry: Industry) {
    this.industryServ.edit(industry.id, industry).subscribe({
      next: () => {
        this.industries$ = this.industryServ.getAll();
        swal.fire('Industria editada', industry.name, 'success');
      },
      error: (err) => console.error(err),
    });
  }

  delete(id: string) {
    this.industryServ.delete(id).subscribe({
      next: () => {
        this.industries$ = this.industryServ.getAll();
        swal.fire('Industria eliminada', '', 'info');
      },
      error: (err) => console.error(err),
    });
  }
}
