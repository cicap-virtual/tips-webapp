import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModalModule, NgbNavModule, NgbPopoverModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslocoModule } from '@ngneat/transloco';
import { CustomComponentsModule } from '@skooltrak/custom-components';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { TreeviewModule } from 'ngx-treeview';
import { LoadingModalModule } from 'src/app/shared/components/loading-modal/loading-modal.module';
import { TreeViewSelectorModule } from 'src/app/shared/components/tree-view-selector/tree-view-selector.module';
import { SharedModule } from 'src/app/shared/shared.module';

import { AdministratorsComponent } from './administrators/administrators.component';
import { CompaniesHomeComponent } from './companies-home/companies-home.component';
import { CompaniesListComponent } from './companies-list/companies-list.component';
import { CompaniesComponent } from './companies.component';
import { CompaniesRoutingModule } from './companies.routes';
import { CompanyDetailsComponent } from './company-details/company-details.component';
import { CompanyOrganizationComponent } from './company-organization/company-organization.component';
import { CompanyProfilesComponent } from './company-profiles/company-profiles.component';
import { CompanySchedulesComponent } from './company-schedules/company-schedules.component';
import { IndustriesComponent } from './industries/industries.component';
import { ProfilesComponent } from './profiles/profiles.component';
import { SchedulesEditComponent } from './schedules-edit/schedules-edit.component';
import { SchedulesFormComponent } from './schedules-form/schedules-form.component';
import { SchedulesNewComponent } from './schedules-new/schedules-new.component';
import { MassiveChargeComponent } from './massive-charge/massive-charge.component';

@NgModule({
  declarations: [
    CompaniesComponent,
    CompaniesHomeComponent,
    IndustriesComponent,
    CompaniesListComponent,
    ProfilesComponent,
    CompanyDetailsComponent,
    CompanyOrganizationComponent,
    CompanyProfilesComponent,
    CompanySchedulesComponent,
    SchedulesFormComponent,
    SchedulesNewComponent,
    SchedulesEditComponent,
    AdministratorsComponent,
    MassiveChargeComponent
  ],
  imports: [
    CommonModule,
    CompaniesRoutingModule,
    ReactiveFormsModule,
    NgbModalModule,
    LoadingModalModule,
    NgbNavModule,
    SharedModule,
    NgbPopoverModule,
    TreeViewSelectorModule,
    TreeviewModule,
    NgxChartsModule,
    TranslocoModule,
    CustomComponentsModule
  ],
  providers: [DatePipe]
})
export class CompaniesModule {}
