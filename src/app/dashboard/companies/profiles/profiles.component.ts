import { Component, OnInit } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { TableOptions } from '@skooltrak/custom-components';
import { Observable } from 'rxjs';
import { Company, Position } from 'src/app/shared/models/companies.model';
import { Profile } from 'src/app/shared/models/surveys.model';
import { CompaniesService } from 'src/app/shared/services/companies.service';
import { PositionsService } from 'src/app/shared/services/positions.service';
import { ProfilesService } from 'src/app/shared/services/profiles.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.sass'],
})
export class ProfilesComponent implements OnInit {
  table = new TableOptions();
  profiles$: Observable<Profile[]>;
  companies$: Observable<Company[]>;
  positions$: Observable<Position[]>;

  constructor(
    private readonly profileServ: ProfilesService,
    private readonly companyServ: CompaniesService,
    private readonly positionServ: PositionsService,
    private transloco: TranslocoService
  ) {}

  ngOnInit() {
    this.companies$ = this.companyServ.getAll();
    this.positions$ = this.positionServ.getAll();
    this.table.title = this.transloco.translate('Profiles');
    this.table.lookup = true;
    this.table.searchable = true;
    this.table.columns = [
      { name: 'name', title: 'Nombre', filterable: true, required: true },
      { name: 'email', title: 'Email', type: 'email', required: true },
      {
        name: 'company',
        title: 'Empresa',
        required: true,
        lookup: true,
        type: 'object',
        asyncList: this.companies$,
        listDisplay: 'name',
        objectText: 'name',
        objectColumn: 'company.name',
      },
      {
        name: 'position',
        title: 'Cargo',
        lookup: true,
        type: 'object',
        readonly: true,
        listDisplay: 'name',
        objectText: 'name',
        objectColumn: 'position.name',
      },
      {
        name: 'registerDate',
        title: this.transloco.translate('registerDate'),
        readonly: true,
        hidden: true,
        type: 'datetime',
      },
    ];

    this.profiles$ = this.profileServ.getProfiles();
  }

  editProfile(profile: Profile) {
    this.profileServ.editProfile(profile.id, profile).subscribe({
      next: (res) => {
        swal.fire('Colaborador actualizado exitosamente', '', 'success');
        this.profiles$ = this.profileServ.getProfiles();
      },
    });
  }
}
