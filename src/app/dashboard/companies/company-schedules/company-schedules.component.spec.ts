import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CompanySchedulesComponent } from './company-schedules.component';

describe('CompanySchedulesComponent', () => {
  let component: CompanySchedulesComponent;
  let fixture: ComponentFixture<CompanySchedulesComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanySchedulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanySchedulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
