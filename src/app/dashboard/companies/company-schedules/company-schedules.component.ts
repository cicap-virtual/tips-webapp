import { Component, Input, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { Organization } from 'src/app/shared/models/companies.model';
import { Schedule } from 'src/app/shared/models/surveys.model';
import { CompaniesService } from 'src/app/shared/services/companies.service';
import { SchedulesNewComponent } from '../schedules-new/schedules-new.component';

@Component({
  selector: 'app-company-schedules',
  templateUrl: './company-schedules.component.html',
  styleUrls: ['./company-schedules.component.sass'],
})
export class CompanySchedulesComponent implements OnInit {
  @Input() companyId: string;

  schedules$: Observable<Schedule[]>;
  constructor(
    private companiesServ: CompaniesService,
    public modal: NgbModal
  ) {}

  ngOnInit() {
    this.schedules$ = this.companiesServ.getSchedules(this.companyId);
  }

  newSchedule() {
    const modalRef = this.modal.open(SchedulesNewComponent, { size: 'lg' });
    modalRef.componentInstance.companyId = this.companyId;
    modalRef.result.then(() => {
      this.schedules$ = this.companiesServ.getSchedules(this.companyId);
    });
  }
}
