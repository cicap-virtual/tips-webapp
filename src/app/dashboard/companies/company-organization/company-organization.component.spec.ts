import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { CompanyOrganizationComponent } from './company-organization.component';

describe('CompanyOrganizationComponent', () => {
  let component: CompanyOrganizationComponent;
  let fixture: ComponentFixture<CompanyOrganizationComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyOrganizationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyOrganizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
