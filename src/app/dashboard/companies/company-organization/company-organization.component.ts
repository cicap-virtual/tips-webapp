import { Component, Input, OnInit } from '@angular/core';
import { TranslocoService } from '@ngneat/transloco';
import { TableOptions } from '@skooltrak/custom-components';
import { Observable } from 'rxjs';
import { Hierarchy } from 'src/app/shared/models/companies.model';
import { CompaniesService } from 'src/app/shared/services/companies.service';
import { HierarchyService } from 'src/app/shared/services/hierarchy.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-company-organization',
  templateUrl: './company-organization.component.html',
  styleUrls: ['./company-organization.component.sass'],
})
export class CompanyOrganizationComponent implements OnInit {
  @Input() companyId: string;

  table = new TableOptions();
  hierarchies$: Observable<Hierarchy[]>;

  constructor(
    private companyServ: CompaniesService,
    private hierarchyServ: HierarchyService,
    private transloco: TranslocoService
  ) {}

  ngOnInit() {
    this.table.creatable = true;
    this.table.exportToCSV = true;
    this.hierarchies$ = this.companyServ.getHierarchy(this.companyId);

    this.table.columns = [
      {
        name: 'isPosition',
        title: this.transloco.translate('position'),
        type: 'boolean',
      },
      { name: 'name', title: 'Nombre', required: true, filterable: true },
      { name: 'code', title: 'Código', filterable: true },
      {
        name: 'parent',
        title: this.transloco.translate('parent'),
        type: 'object',
        asyncList: this.hierarchies$,
        removeSelf: true,
      },
      {
        name: 'description',
        title: this.transloco.translate('description'),
        type: 'text',
        hidden: true,
      },
    ];
  }

  createHierarchy(hierarchy: Hierarchy) {
    hierarchy.companyId = this.companyId;
    this.hierarchyServ.create(hierarchy).subscribe({
      next: (res) => {
        swal.fire('Jerarquía creada exitosamente', res.name, 'success');
        this.hierarchies$ = this.companyServ.getHierarchy(this.companyId);
      },
      error: (err) =>
        swal.fire(
          this.transloco.translate('Something went wrong'),
          err.message,
          'error'
        ),
    });
  }

  editHierarchy(hierarchy: Hierarchy) {
    this.hierarchyServ.edit(hierarchy.id, hierarchy).subscribe({
      next: () => {
        swal.fire('Jerarquí editada exitosamente', hierarchy.name, 'success');
        this.hierarchies$ = this.companyServ.getHierarchy(this.companyId);
      },
      error: (err) =>
        swal.fire(
          this.transloco.translate('Something went wrong'),
          err.message,
          'error'
        ),
    });
  }
}
