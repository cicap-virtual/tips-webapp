import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { AngularFireModule } from '@angular/fire/compat';
import { AngularFirestoreModule } from '@angular/fire/compat/firestore';
import { RouterTestingModule } from '@angular/router/testing';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { TranslocoModule } from '@ngneat/transloco';
import { environment } from 'src/environments/environment';

import { AppComponent } from './app.component';

fdescribe('AppComponent', () => {
  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [
          RouterTestingModule,
          TranslocoModule,
          AngularFireModule.initializeApp(environment.firebase),
          AngularFirestoreModule,
          HttpClientTestingModule,
          NgIdleKeepaliveModule.forRoot()
        ],
        declarations: [AppComponent],
        providers: []
      }).compileComponents();
    })
  );

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
