import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Survey } from '../shared/models/surveys.model';
import { MarketService } from '../shared/services/markets.service';

@Component({
  selector: 'app-market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.sass'],
})
export class MarketComponent implements OnInit {
  surveys$: Observable<Survey[]>;
  constructor(private marketService: MarketService) {}

  ngOnInit(): void {}
}
