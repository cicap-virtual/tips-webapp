import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslocoService } from '@ngneat/transloco';
import { AuthService } from 'src/app/shared/services/auth.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.sass']
})
export class SignInComponent implements OnInit {
  loginForm: FormGroup;
  constructor(
    private auth: AuthService,
    private fb: FormBuilder,
    private transloco: TranslocoService
  ) {
    this.auth.signOut();
    swal.fire({
      toast: true,
      position: 'top-right',
      icon: 'info',
      text: this.transloco.translate('Logged out successfully'),
      timer: 3000,
      showConfirmButton: false
    });
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  signIn() {
    this.auth.signIn(
      this.loginForm.get('email').value,
      this.loginForm.get('password').value
    );
  }

  signGoogle() {
    this.auth.googleAuth();
  }
}
