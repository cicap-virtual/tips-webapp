import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslocoModule } from '@ngneat/transloco';

import { AuthComponent } from './auth.component';
import { AuthRoutingModule } from './auth.routes';
import { RolesComponent } from './roles/roles.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { WaitingAccessComponent } from './waiting-access/waiting-access.component';

@NgModule({
  declarations: [
    AuthComponent,
    SignInComponent,
    SignUpComponent,
    VerifyEmailComponent,
    WaitingAccessComponent,
    RolesComponent,
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    TranslocoModule,
    FormsModule,
    ReactiveFormsModule,
  ],
})
export class AuthModule {}
