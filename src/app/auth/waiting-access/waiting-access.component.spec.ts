import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { WaitingAccessComponent } from './waiting-access.component';

describe('WaitingAccessComponent', () => {
  let component: WaitingAccessComponent;
  let fixture: ComponentFixture<WaitingAccessComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ WaitingAccessComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WaitingAccessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
