import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from 'src/app/shared/services/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.sass']
})
export class SignUpComponent implements OnInit {
  loginForm: FormGroup;
  constructor(private auth: AuthService, private fb: FormBuilder) {}

  ngOnInit() {
    this.loginForm = this.fb.group(
      {
        email: ['', [Validators.email, Validators.required]],
        password: ['', [Validators.required]],
        passwordConfirm: ['', [Validators.required]]
      },
      { validators: [this.passwordConfirm] }
    );
  }

  signUp() {
    this.auth.signUp(
      this.loginForm.get('email').value,
      this.loginForm.get('password').value
    );
  }

  signGoogle() {
    this.auth.googleAuth();
  }

  passwordConfirm(g: FormGroup) {
    return g.get('password').value === g.get('passwordConfirm').value
      ? null
      : { mismatch: true };
  }
}
