import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { selectRole } from 'src/app/store/actions';
import { selectRoles } from 'src/app/store/selectors';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.sass'],
})
export class RolesComponent implements OnInit {
  form: FormGroup;
  roles$ = this.store$.select(selectRoles);
  constructor(private store$: Store, private fb: FormBuilder) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      role: ['', Validators.required],
    });
  }

  selectRole(): void {
    const { role } = this.form.value;
    this.store$.dispatch(selectRole({ role }));
  }
}
