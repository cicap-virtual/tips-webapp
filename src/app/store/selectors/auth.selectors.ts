import { createSelector } from '@ngrx/store';

import { AppState } from '../app.reducer';
import { AuthState } from '../reducers';

const _selectAuth = (state: AppState) => state.auth;

export const selectAuth = createSelector(
  _selectAuth,
  (state: AuthState) => state
);

export const selectUser = createSelector(
  _selectAuth,
  (state: AuthState) => state.user
);

export const selectRoles = createSelector(
  _selectAuth,
  (state: AuthState) => state.user?.roles
);

export const selectProfile = createSelector(
  _selectAuth,
  (state: AuthState) => state.profile
);

export const selectOtherRoles = createSelector(
  _selectAuth,
  (state: AuthState) =>
    state.user?.roles.filter((x) => x.id !== state.currentRole.id)
);

export const selectAuthInfo = createSelector(
  _selectAuth,
  (state: AuthState) => state.authInfo
);

export const selectIsLogged = createSelector(
  _selectAuth,
  (state: AuthState) => state.logged
);

export const selectCurrentRole = createSelector(
  _selectAuth,
  (state: AuthState) => state.currentRole
);
