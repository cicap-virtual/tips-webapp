import { createAction, props } from '@ngrx/store';
import { Auth, Role, User } from 'src/app/shared/models/auth.models';
import { Profile } from 'src/app/shared/models/surveys.model';

export enum AuthActions {
  LoginSuccess = '[AUTH] Login success',
  SelectRole = '[AUTH] Select Role',
  ChangeRole = '[AUTH] Change Role',
  UpdateRole = '[AUTH] Update Role',
}

export const loginSuccess = createAction(
  AuthActions.LoginSuccess,
  props<{ user: User; authInfo: Auth; profile: Profile }>()
);

export const selectRole = createAction(
  AuthActions.SelectRole,
  props<{ role: Role }>()
);

export const updateRole = createAction(
  AuthActions.UpdateRole,
  props<{ role: Role }>()
);

export const changeRole = createAction(
  AuthActions.ChangeRole,
  props<{ role: Role }>()
);
