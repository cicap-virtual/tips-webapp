import { Action, createReducer, on } from '@ngrx/store';
import { Auth, Role, User } from 'src/app/shared/models/auth.models';
import { Profile } from 'src/app/shared/models/surveys.model';

import { loginSuccess, selectRole, updateRole } from '../actions';

export interface AuthState {
  user: User | undefined;
  profile: Profile | undefined;
  authInfo: Auth | undefined;
  logged: boolean;
  currentRole: Role;
}

const initialState: AuthState = {
  user: undefined,
  authInfo: undefined,
  profile: undefined,
  logged: false,
  currentRole: undefined,
};

const _authReducer = createReducer(
  initialState,
  on(
    loginSuccess,
    (state, { user, authInfo, profile }): AuthState => ({
      ...state,
      user,
      authInfo,
      profile,
      logged: true,
    })
  ),
  on(
    selectRole,
    (state, { role }): AuthState => ({ ...state, currentRole: role })
  ),
  on(
    updateRole,
    (state, { role }): AuthState => ({ ...state, currentRole: role })
  )
);

export const authReducer = (state: AuthState | undefined, action: Action) =>
  _authReducer(state, action);
