import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { exhaustMap, map, tap } from 'rxjs';
import { RolesService } from 'src/app/shared/services/roles.service';

import * as actions from '../actions';

@Injectable()
export class AuthEffects {
  loginSucces$ = createEffect(
    () => {
      return this.actions$.pipe(
        ofType(actions.loginSuccess),
        tap(() => {
          this.router.navigate(['/login/roles']);
        })
      );
    },
    { dispatch: false }
  );

  selectRole$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(actions.selectRole),
      tap((value) => {
        const { role } = value;
        if (role.isAdmin || role.isCompanyAdmin || role.isCreator) {
          this.router.navigate(['/dashboard']);
        } else {
          this.router.navigate(['/app']);
        }
      }),
      exhaustMap((value) =>
        this.rolesService
          .getRole(value.role.id)
          .pipe(map((res) => actions.updateRole({ role: res })))
      )
    );
  });
  constructor(
    private actions$: Actions,
    private router: Router,
    private rolesService: RolesService
  ) {}
}
