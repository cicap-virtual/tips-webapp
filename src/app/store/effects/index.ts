import { AuthEffects } from './auth.effects';

export * from './auth.effects';
export const appEffects = [AuthEffects];
